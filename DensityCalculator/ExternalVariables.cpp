//
// Created by Robert Starr on 5/16/20.
//

#include "ExternalVariables.h"
#include <fstream>

ExternalVariables::ExternalVariables() {
    baseNanoflowDir = "../../";
    externalVariableFile = baseNanoflowDir + "variables.txt";
    atomDirectory = baseNanoflowDir + "Frames/";
    setVariableFromFile("WALLSTART", &wallStart);
    setVariableFromFile("INNERRADIUS", &radius);
    setVariableFromFile("HEIGHT", &wallEnd);
}

float ExternalVariables::getWallStart() {
    return wallStart;
}

float ExternalVariables::getRadius() {
    return radius;
}

void ExternalVariables::setVariableFromFile(string variableInFile, float* variable) {
    *variable = stof(split(findVariableLine(variableInFile, externalVariableFile))[3]);
}

string ExternalVariables::getAtomDirectory() { return atomDirectory; }
string ExternalVariables::findVariableLine(string variable, string filename) {
    string line;
    ifstream file(filename);

    while(getline(file, line)) {
        vector<string> splitLine = split(line);
        if (!splitLine[1].compare(variable)) return line;
    }

    return std::string();
}

vector<string> ExternalVariables::split(string a){
    return split(a, "\t");
}

vector<string> ExternalVariables::split(string inputString, string breakCharacters) {
    vector<string> results;
    string temp;
    string delimiter = " " + breakCharacters;
    size_t pos = 0;
    while ((pos = inputString.find_first_of(delimiter)) != string::npos) {
        temp = inputString.substr(0, pos);
        results.push_back(temp);
        inputString.erase(0, pos + delimiter.length() - 1);
    }
    results.push_back(inputString);
    return results;
}

float ExternalVariables::getWallEnd() {
    return wallEnd;
}
