//
// Created by Robert Starr on 5/16/20.
//

#ifndef DENSITYCALCULATOR_EXTERNALVARIABLES_H
#define DENSITYCALCULATOR_EXTERNALVARIABLES_H

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class ExternalVariables {

public:
    ExternalVariables();
    float getWallStart();
    float getRadius();
    float getWallEnd();
    string getAtomDirectory();
    static vector<string> split(string, string);

private:
    float wallStart, wallEnd, radius;
    string baseNanoflowDir, externalVariableFile, atomDirectory;

    void setVariableFromFile(string, float*);
    vector<string> split(string);
    string findVariableLine(string);
    string findVariableLine(string variable, string filename);
};


#endif //DENSITYCALCULATOR_EXTERNALVARIABLES_H
