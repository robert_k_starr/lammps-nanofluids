//
// Created by Robert Starr on 5/16/20.
//

#include "AtomDensitiesFactory.h"

AtomDensitiesFactory::AtomDensitiesFactory() {

}

AtomDensitiesFactory AtomDensitiesFactory::withBinWidth(float binWidth) {
    AtomDensitiesFactory newAtomDensitiesFactory = AtomDensitiesFactory(*this);
    newAtomDensitiesFactory.setBinWidth(binWidth);
    return newAtomDensitiesFactory;
}

AtomDensitiesFactory AtomDensitiesFactory::withBinStart(float binStart) {
    AtomDensitiesFactory newAtomDensitiesFactory = AtomDensitiesFactory(*this);
    newAtomDensitiesFactory.setBinStart(binStart);
    return newAtomDensitiesFactory;
}

AtomDensitiesFactory AtomDensitiesFactory::withNumberOfBins(int numberOfBins) {
    AtomDensitiesFactory newAtomDensitiesFactory = AtomDensitiesFactory(*this);
    newAtomDensitiesFactory.setNumberOfBins(numberOfBins);
    return newAtomDensitiesFactory;
}

AtomDensitiesFactory AtomDensitiesFactory::withSourceFile(string sourceFile) {
    AtomDensitiesFactory newAtomDensitiesFactory = AtomDensitiesFactory(*this);
    newAtomDensitiesFactory.setSourceFile(sourceFile);
    return newAtomDensitiesFactory;
}

void AtomDensitiesFactory::setBinWidth(float binWidth) {
    this->binWidth = binWidth;
}

void AtomDensitiesFactory::setBinStart(float binStart) {
    this->binStart = binStart;
}

void AtomDensitiesFactory::setNumberOfBins(int numberOfBins) {
    this->numberOfBins = numberOfBins;
}

void AtomDensitiesFactory::setSourceFile(const string &sourceFile) {
    AtomDensitiesFactory::sourceFile = sourceFile;
}

AtomDensities AtomDensitiesFactory::build() {
    return AtomDensities(sourceFile, binWidth, binStart);
}


