//
// Created by Robert Starr on 5/16/20.
//

#ifndef DENSITYCALCULATOR_ATOMDENSITIESCOLLECTION_H
#define DENSITYCALCULATOR_ATOMDENSITIESCOLLECTION_H

#include <vector>
#include <string>
#include "ExternalVariables.h"
#include "AtomDensities.h"
#include "Parameters.h"

using namespace std;

class AtomDensitiesCollection {
public:
    AtomDensitiesCollection(ExternalVariables);
    void buildCollection();
    string generateOutputFile();
private:
    ExternalVariables externalVariables;
    vector<AtomDensities> atomDensities;
    int numberOfBins;
    float binWidth;
    float calculateBinWidth();
    vector<string> atomFiles;
    void initiateAtomFiles();
    void outputFile(string filename, string textToWrite);
};


#endif //DENSITYCALCULATOR_ATOMDENSITIESCOLLECTION_H
