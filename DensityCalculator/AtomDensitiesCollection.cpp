//
// Created by Robert Starr on 5/16/20.
//

#include "AtomDensitiesCollection.h"
#include <dirent.h>
#include <fstream>
#include "AtomDensitiesFactory.h"

AtomDensitiesCollection::AtomDensitiesCollection(ExternalVariables externalVariables) {
    this->externalVariables = externalVariables;
    this->numberOfBins = NUMBER_OF_BINS;
    this->binWidth = calculateBinWidth();
}

void AtomDensitiesCollection::initiateAtomFiles() {
        DIR *dir;
        struct dirent *ent;
        vector<string> files;
        string path = externalVariables.getAtomDirectory();
        const char *directory = path.c_str();
        if ((dir = opendir(directory)) != NULL) {
            while ((ent = readdir(dir)) != NULL) {
                string temp = ent->d_name;
                if (temp.compare(0, 3, "xyz") == 0) files.push_back(path + temp);
            }
            closedir(dir);
        }

        atomFiles = files;
}

void AtomDensitiesCollection::buildCollection() {
    initiateAtomFiles();

    AtomDensitiesFactory atomDensitiesFactory = AtomDensitiesFactory()
            .withBinStart(externalVariables.getWallStart())
            .withBinWidth(binWidth);

    for (int i=0; i < atomFiles.size(); i++) {
        cout << i << "/" << atomFiles.size() << "\n";
        atomDensities.push_back(
                atomDensitiesFactory
                .withSourceFile(atomFiles[i])
                .build()
                );
    }
}

float AtomDensitiesCollection::calculateBinWidth() {
    return (externalVariables.getWallEnd() - externalVariables.getWallStart())/numberOfBins;
}

string AtomDensitiesCollection::generateOutputFile() {
    string output = "xAxis\t";
    for (int i = 0; i < numberOfBins; i++) {
        output += to_string(externalVariables.getWallStart() + i*binWidth) + "\t";
    }
    output += "\n";
    for (int i = 0; i < atomDensities.size(); i++) {
        output += atomDensities[i].getFileName() + "\t" + atomDensities[i].getBinsAsString();
    }

    outputFile("../outputFile.txt", output);

    return output;
}

void AtomDensitiesCollection::outputFile(string filename, string textToWrite){
    ofstream file;

    file.open(filename,ios::trunc);
    file.close();

    file.open(filename, ios::app);
    file << textToWrite;
    file.close();
}