//
// Created by Robert Starr on 5/16/20.
//

#include "AtomDensities.h"
#include "ExternalVariables.h"
#include <cmath>
#include <fstream>

AtomDensities::AtomDensities(string sourceFile, float binWidth, float binStart) {
    this->sourceFile = sourceFile;
    this->binWidth = binWidth;
    this->binStart = binStart;
    this->numberOfBins = NUMBER_OF_BINS;

    for (int i=0; i < numberOfBins; i++) bins[i] = 0;

    calculateBins();
}

int AtomDensities::getBin(float xCoordinate) {
    return floor((xCoordinate - binStart)/binWidth);
}
float AtomDensities::getXCoordinate(string line) {
    vector<string> splitLine = ExternalVariables::split(line, " ");
    if (splitLine.size() == 6) {
        return stof(splitLine[2]);
    }
}

int AtomDensities::getBin(string line) {
    return getBin(getXCoordinate(line));
}

void AtomDensities::calculateBins() {
    string line;
    ifstream file(sourceFile);
    while(getline(file, line)) {
        if (!line.compare("ITEM: ATOMS id type x y z ")) break;
    }
    while(getline(file, line)) {
        int bin = getBin(line);
        if ((getType(line) == OXYGEN_ATOM) && (bin >= 0) && (bin < numberOfBins)) {
            bins[bin] = bins[bin] + 1;
        };
    }
}

void AtomDensities::printBins() {
    cout << getBinsAsString();
}



int AtomDensities::getType(string line) {
    return stoi(ExternalVariables::split(line, " ")[1]);
}

string AtomDensities::getBinsAsString() {
    string output = "";
    for (int i=0; i<numberOfBins; i++) {
        output = output + to_string(bins[i]) + "\t";
    }

    return output + "\n";
}

int AtomDensities::getFileNumber() {
    vector<string> splitSourceFiles = ExternalVariables::split(sourceFile, "/");
    string fileName = splitSourceFiles[splitSourceFiles.size()-1];
    int locationOfPeriodInFileName = fileName.find(".");
    string outputFileNumber = "";
    for (int i = 3; i < locationOfPeriodInFileName; i++) outputFileNumber = outputFileNumber + fileName[i];

    return stoi(outputFileNumber);
}

string AtomDensities::getFileName() {
    vector<string> splitSourceFile = ExternalVariables::split(sourceFile, "/");
    return splitSourceFile[splitSourceFile.size()-1];
}
