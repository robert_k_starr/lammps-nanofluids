#include <iostream>
#include "ExternalVariables.h"
#include "AtomDensitiesCollection.h"

int main() {
    ExternalVariables externalVariables;
    AtomDensitiesCollection atomDensities(externalVariables);
    atomDensities.buildCollection();
    cout << atomDensities.generateOutputFile();

    return 0;
}
