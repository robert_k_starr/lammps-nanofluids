//
// Created by Robert Starr on 5/16/20.
//

#ifndef DENSITYCALCULATOR_ATOMDENSITIESFACTORY_H
#define DENSITYCALCULATOR_ATOMDENSITIESFACTORY_H


#include "AtomDensities.h"
#include <string>

using namespace std;

class AtomDensitiesFactory {
public:
    AtomDensitiesFactory();
    AtomDensitiesFactory withBinWidth(float);
    AtomDensitiesFactory withBinStart(float);
    AtomDensitiesFactory withNumberOfBins(int);
    AtomDensitiesFactory withSourceFile(string);

    void setBinWidth(float);
    void setBinStart(float);
    void setNumberOfBins(int);
    void setSourceFile(const string &sourceFile);

    AtomDensities build();
private:
    float binWidth, binStart;
    int numberOfBins;
    string sourceFile;
};


#endif //DENSITYCALCULATOR_ATOMDENSITIESFACTORY_H
