//
// Created by Robert Starr on 5/16/20.
//

#ifndef DENSITYCALCULATOR_ATOMDENSITIES_H
#define DENSITYCALCULATOR_ATOMDENSITIES_H

#include<string>
#include "Parameters.h"

using namespace std;

class AtomDensities {
public:
    AtomDensities(string, float, float);
    int getFileNumber();
    string getFileName();
    string getBinsAsString();

private:
    int bins[NUMBER_OF_BINS], numberOfBins;
    float binWidth, binStart;
    string sourceFile;

    float getXCoordinate(string);
    void calculateBins();
    int getBin(string);
    int getBin(float);
    int getType(string);
    void printBins();
};


#endif //DENSITYCALCULATOR_ATOMDENSITIES_H
