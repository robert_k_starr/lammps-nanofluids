import numpy as np
import matplotlib.pyplot as plt
import random
import math
import os

filename = "outputFile.txt"
outputfolder = "Images"
framesFolder = "../Frames/"
variableFile = "../variables.txt"
HEIGHT = 10
WIDTH = HEIGHT*16/9.0
OXYGEN_ATOM = 3

file = open(filename, 'r')

def getRadius():
	variables = open(variableFile, 'r')
	for line in variables:
		splitLine = line.split()
		if (len(splitLine) != 4):
			continue
		if (splitLine[1] == "POSRADIUS"):
			return int(splitLine[3])

radius = getRadius()

def highestValueInArray(line):
	maxValue = 0
	for i in range(0, len(line)):
		if (float(line[i]) > maxValue):
			maxValue = float(line[i])
	return maxValue

def getCoordinates(line):
	return (float(line[2]), float(line[3]), float(line[4]))
	
def extractOxygenCoordinatesFromFile(atomFile):
	openedAtomFile = open(atomFile, 'r')
	atomCoordinates = []
	for line in openedAtomFile:
		fullData = line.split()
		if ((len(fullData) == 5) and (int(fullData[1]) == OXYGEN_ATOM)):
			atomCoordinates.append(getCoordinates(fullData))
	return atomCoordinates

def xValues(atoms):
	finalXValues = []
	for atom in atoms:
		finalXValues.append(atom[0])
	return finalXValues

def normalizedR(atom):
	rSquared = pow(atom[1], 2) + pow(atom[2], 2)
	sign = 1
	if (atom[1] < 0):
		sign = -1
	return sign*math.sqrt(rSquared)/(2*radius) + .5
	
def normalizedRValues(atoms):
	finalRValues = []
	for atom in atoms:
		finalRValues.append(normalizedR(atom))
	return finalRValues

def removeExtension(stringWithExtension):
	return stringWithExtension.split(".")[0]
	
outputDictionary = {}
xAxis = []
for line in file:
	temp = line.split()
	key = temp[0]
	if (key == 'xAxis'):
		for i in range(1, len(temp)):
			xAxis.append(float(temp[i]))
		continue
	tempArray = []
	maxValue = highestValueInArray(temp[1:])
	if (maxValue == 0):
		continue
	for i in range(1, len(temp)):
		try:
			if (float(temp[i]) == 0):
				tempArray.append(-1)
			else:
				tempArray.append(float(temp[i])/maxValue)
		except:
			continue
	outputDictionary[key] = tempArray

itemsToGraph = sorted(outputDictionary.items())

if not os.path.exists(outputfolder):
	os.makedirs(outputfolder)
os.system("rm -r " + outputfolder + "/*")

print("Graphing...")
for item in itemsToGraph:
	atoms = extractOxygenCoordinatesFromFile(framesFolder + item[0])
	fig = plt.figure(1, figsize=(WIDTH,HEIGHT))
	ax1 = fig.add_subplot(111)
	ax1.plot(xValues(atoms), normalizedRValues(atoms), 'bo', markersize=8)
	ax1.plot(xAxis,item[1],'rs', markersize = 10)
	plt.xlim(50, 200)
	plt.ylim(0,1)
	ax = plt.gca()
	ax.set_facecolor('black')

	plt.savefig(outputfolder + '/' + removeExtension(item[0]) + '.png')
	fig = plt.close()
	
os.system("convert -delay 2 -quality 100 " + outputfolder + "/*.png " + outputfolder + "/file.mpeg")