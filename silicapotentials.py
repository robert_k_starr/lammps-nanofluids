mole = 6.022*pow(10,23)
filename = "silicapotentials.txt"

output = open(filename, 'w')

def j2kc(joules):
	return joules*pow(10,-21)*mole*0.000239006
	
def nm2a(nano):
	return nano*10

def Cconvert(cvalue):
	return j2kc(cvalue)*pow(10,6)
	
def printfile(particle1, particle2, A, rho, sigma, C=0, D=0):
	line = "pair_coeff\t${" + particle1 + "}\t${" + particle2 + "}\t"
	line = line + str(j2kc(A)) + "\t" + str(nm2a(rho)) + "\t" + str(nm2a(sigma)) + "\t"
	line = line + str(Cconvert(C)) + "\t" + str(D) + "\n"
	output.write(line)

printfile('SILICA_OXYGEN', 'SILICA_OXYGEN', 4.05, .284, .029)
printfile('SILICON', 'SILICA_OXYGEN', 26.38, .275, .029)
printfile('SILICON', 'SILICON', 68.4, .266, .029)