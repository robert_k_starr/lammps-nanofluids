#include <iostream>
#include <cstring>
#include <math.h>
#include <time.h>
#include <stdlib.h>

#include "functions.hpp"
#include "parameters.hpp"

atom::atom(){
	atom(-1,0,0,0,0);
}

atom::atom(string line){
    vector<string> temp = split(line);
    id = stoi(temp[0]);
    type = stoi(temp[1]);
    coords[0] = stof(temp[2]);
    coords[1] = stof(temp[3]);
    coords[2] = stof(temp[4]);
    charge = 0;
    numberOfNeighbors = 0;
    deleteatom = false;
}

atom::atom(int atomid, float a, float b, float c, int atomtype, float atomcharge){
	id = atomid;
	type = atomtype;
	coords[0] = a;
	coords[1] = b;
	coords[2] = c;
	charge = atomcharge;
	numberOfNeighbors = 0;
	deleteatom = false;
	
	for (int i = 0; i < 3; i++){
		parent[i] = NULL;
		left[i] = NULL;
		right[i] = NULL;
	}

}

atom::atom(int atomid, float a, float b, float c, int atomtype){
	id = atomid;
	type = atomtype;
	coords[0] = a;
	coords[1] = b;
	coords[2] = c;
	charge = 0;
	numberOfNeighbors = 0;
	deleteatom = false;
	
	for (int i = 0; i < 3; i++){
		parent[i] = NULL;
		left[i] = NULL;
		right[i] = NULL;
	}

}

float atom::distance(atom otherAtom) {
    float unsquaredDistance = 0;
    for (int i = 0; i < 3; i++) unsquaredDistance += pow(coords[i] - otherAtom.coords[i], 2);

    return sqrt(unsquaredDistance);
}
void atom::delete_atom(){
	deleteatom = 1;
}

void atom::undelete_atom(){
	deleteatom = 0;
}

vector<string> split(string a){
	vector<string> results;
	string temp;
	string delimiter = " ";
	size_t pos = 0;
    while ((pos = a.find(delimiter)) != string::npos) {
        temp = a.substr(0, pos);
        results.push_back(temp);
        a.erase(0, pos + delimiter.length());
    }
    results.push_back(a);
    return results;
}

void atom::set_parent(atom &parentatom, int i){
	parent[i] = &parentatom;
}

void atom::set_daughter(atom& daughteratom, int i){
	if (coords[i] > daughteratom.coords[i]){
		if (left[i] == NULL) {
			left[i] = &daughteratom;
			daughteratom.set_parent(*this, i);
			}
		else left[i]->set_daughter(daughteratom, i);
	}
	else{
		if (right[i] == NULL) {
			right[i] = &daughteratom;
			daughteratom.set_parent(*this,i);
		}
		else right[i]->set_daughter(daughteratom, i);
	}
}

void atom::location(){
	cout << id << ": (";
	for (int i = 0; i < 3; i++) cout << coords[i] << ", ";
	cout << "\b\b)\n";
}

void atom::reveal_parent(int i){
	parent[i]->location();
}

float atom::distancefromcenter(){
	return sqrt(pow(coords[1],2) + pow(coords[2],2));
}

bool atom::is_deleted(){
	return deleteatom;
}

void atom::set_id(int i){
	id = i;
}

string atom::output(){
	string temp;
	
	temp = to_string(id);

	temp = temp + " " + to_string(type) + " ";
	for (int i = 0; i< 3; i++){
		temp = temp + to_string(coords[i]) + " ";
	}
	temp = temp + "0 " + to_string(charge) + " 0 0 0 0";
	return temp;
}

void atom::set_type(int new_type){
	type = new_type;
}

void atom::set_charge(float new_charge){
	charge = new_charge;
}

void atom::addNeighbor(atom &newNeighbor) {
    bool uniqueNeighbor = true;
    for (int i = 0; i < numberOfNeighbors; i++) {
        if (neighbors[i] == &newNeighbor) {
            uniqueNeighbor = false;
        }
    }
    if (uniqueNeighbor) {
        neighbors[numberOfNeighbors++] = &newNeighbor;
    }
}

int atom::getNumberOfNeighbors() {
    return numberOfNeighbors;
}

float atom::getCharge() {
    return charge;
}

int atom::getNumberOfNeighborsOfType(int neighborType) {
    int count = 0;
    for (int i = 0; i < numberOfNeighbors; i++){
        if (neighbors[i]->is_deleted()) continue;
        if (neighbors[i]->type == neighborType) count++;
    }
    return count;
}

atom* atom::getEdgeSiliconNeighbor() {
    for (int i = 0; i < numberOfNeighbors; i++) {
        if ((neighbors[i]->getType() == EDGE_SILICON) && (!neighbors[i]->is_deleted())) {
            return neighbors[i];
        }
    }
    return nullptr;
}

int atom::getType() {
    return type;
}

void add_roughness(atom source, float radius){
	atom temp = source;
	while ((temp.coords[0] - source.coords[0] < radius)||(temp.coords[0] - source.coords[0] > -1*radius)){
		if (temp.parent[0] != NULL){
			temp = *temp.parent[0];
		}
		else break;
	}
	
	undelete_daughters(temp, source, radius);
}

int distance(atom a, atom b){
	float sumsq = 0;
	for (int i = 0; i < 3; i++){
		sumsq += pow(a.coords[i]-b.coords[i],2);
	}
	return sqrt(sumsq);
}

void undelete_daughters(atom &test, atom &original, float radius){
	if (distance(test, original) < radius){
		test.undelete_atom();
	}
	
	float temp = test.coords[0] - original.coords[0];
	
	if (temp < radius){
		if (test.right[0] != NULL) undelete_daughters(*test.right[0],original,radius);
	}
	if (temp > -1*radius){
		if (test.left[0] != NULL) undelete_daughters(*test.left[0], original, radius);
	}	
}

vector<atom *> return_surrounding_atoms(atom start_atom, float radius){
	vector<atom*> temp;
	atom *current_parent = start_atom.parent[0];
	
	// Find furthest parent within radius
	while ((current_parent != NULL)&&
		(abs(current_parent[0].coords[0] - start_atom.coords[0]) <= radius)){
		current_parent = current_parent->parent[0];
		}
	
	temp = return_surrounding_daughters(*current_parent, start_atom, radius);
	
	return temp;
}

vector<atom *> return_surrounding_daughters(atom start_atom, atom center_atom, float radius) {
	vector<atom *> temp_left;
	vector<atom *> temp_right;

	if (start_atom.left[0]){
		temp_left = return_surrounding_daughters(*start_atom.left[0], center_atom, radius);
	}
	if (start_atom.right[0]){
		temp_right = return_surrounding_daughters(*start_atom.right[0], center_atom, radius);
	}
	
	vector<atom *> temp_final;
	
	temp_final.reserve(temp_left.size() + temp_right.size());
	temp_final.insert(temp_final.end(), temp_left.begin(), temp_left.end());
	temp_final.insert(temp_final.end(), temp_right.begin(), temp_right.end());
	
	if (distance(start_atom, center_atom) <= radius) temp_final.push_back(&start_atom);
	
	return temp_final;
}

atom new_hydrogen(atom oxygen_atom, atom silicon_atom){
	// A*B = |A||B|cos(theta)
	// Randomly choose x and y
	// (A_x * x + A_y*y)/(A_z*cos(theta)) = z
	// Normalize to HYDROGEN_LENGTH
	float x, y, z;
	float o_x, o_y, o_z;	// Oxygen vectors
	int rand_range = 10000;
	
	o_x = oxygen_atom.coords[0] - silicon_atom.coords[0];
	o_y = oxygen_atom.coords[1] - silicon_atom.coords[1];
	o_z = oxygen_atom.coords[2] - silicon_atom.coords[2];
	
	x = (rand() % rand_range); // Come up with method for randomizing +/-
	y = (rand() % rand_range);
	z = (o_x*x + o_y*y)/(o_z*cos(HYDROGEN_ANGLE));	// Double check that angle should be 
													// in degrees, not radians

	float length = sqrt(pow(x,2) + pow(y,2) + pow(z,2));
	
	x = silicon_atom.coords[0] + x*HYDROGEN_LENGTH/length;
	y = silicon_atom.coords[1] + y*HYDROGEN_LENGTH/length;
	z = silicon_atom.coords[2] + z*HYDROGEN_LENGTH/length;

	return atom(-1, x, y, z, EDGE_HYDROGEN, HYDROGEN_CHARGE);
	//int atomid, float a, float b, float c, int atomtype
}

atom new_hydrogen(atom silicon_atom){
	float x, y, z;
	int rand_range = 10000;
	x = (rand() % rand_range); // Come up with method for randomizing +/-
	y = (rand() % rand_range);
	z = (rand() % rand_range);
	
	// 	Normalize
	float length = sqrt(pow(x,2) + pow(y,2) + pow(z,2));
	
	x = silicon_atom.coords[0] + x*HYDROGEN_LENGTH/length;
	y = silicon_atom.coords[1] + y*HYDROGEN_LENGTH/length;
	z = silicon_atom.coords[2] + z*HYDROGEN_LENGTH/length;
	
	return atom(-1, x, y, z, EDGE_HYDROGEN, HYDROGEN_CHARGE);
	//int atomid, float a, float b, float c, int atomtype
}

int hydroxylate(vector<atom> &atomlist, float inner_radius, float region_thickness){
	srand(time(NULL));
	// Determine region to be checking for hydroxylation 
	// 	(this should come from variables file)
	
	float MIN_DISTANCE = inner_radius;
	float MAX_DISTANCE = MIN_DISTANCE + region_thickness;
	
	float MAX_RADIUS = .95;
	int new_atoms = 0;
	
	int total_atoms = atomlist.size();
	for (int i = 0; i < total_atoms; i++) {
		if ((atomlist[i].distancefromcenter() > MAX_DISTANCE) || 
			(atomlist[i].is_deleted())){
			continue;
		}

		if (atomlist[i].type == SILICA_OXYGEN) {
			atomlist[i].set_type(EDGE_OXYGEN);
			continue;	
		} else if (atomlist[i].type == SILICON){
			atomlist[i].set_type(EDGE_SILICON);
		} else {
			continue;
		}

		vector <atom *> surrounding_atoms = return_surrounding_atoms(atomlist[i], MAX_RADIUS);
		
		int oxygen_count = 0;
		atom* oxygen_atom;
		
		for (int i = 0; i < surrounding_atoms.size(); i++){
			if ((surrounding_atoms[i]->type == SILICA_OXYGEN)||(surrounding_atoms[i]->type == EDGE_OXYGEN)){
				oxygen_count++;
				oxygen_atom = surrounding_atoms[i];
			}
		}
		
		if (oxygen_count == 1 && oxygen_atom->charge == 0){
			atomlist[oxygen_atom->id].set_charge(OXYGEN_CHARGE);
			atomlist[i].set_charge(SILICON_CHARGE);
			atomlist.push_back(new_hydrogen(*oxygen_atom, atomlist[i]));
			new_atoms++;
		} 
		// else if (oxygen_count == 0){
// 			atomlist.push_back(new_hydrogen(atomlist[i]));
// 			new_atoms++;
// 		}
	}
	
	return new_atoms;
}

void re_id_atoms(vector<atom> &atoms){
	int current_id = -1;
	int total_atoms = atoms.size();
	cout << "Total atoms: " << total_atoms << "\n";
	for (int i = 0; i < total_atoms; i++){
		if (!atoms[i].is_deleted()){
			atoms[i].set_id(++current_id);
			atoms[current_id] = atoms[i];
		}
	}
	cout << "Active atoms: " << current_id << "\n\n";
}