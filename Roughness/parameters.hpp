#ifndef parameters_hpp
#define parameters_hpp

#define ROUGHNESS_SIZE 10
#define CHANCE 0

#define SILICON 1
#define SILICA_OXYGEN 2

#define WATER_OXYGEN 3
#define WATER_HYDROGEN 4

#define EDGE_HYDROGEN 5
#define EDGE_OXYGEN 6
#define EDGE_SILICON 7

#define HYDROGEN_ANGLE 116
#define HYDROGEN_LENGTH .95

#define HYDROGEN_CHARGE 0.435
#define OXYGEN_CHARGE -0.700
#define SILICON_CHARGE 0.265

#define NEIGHBOR_DISTANCE 2
#define EDGE_THICKNESS 10
#endif /* parameters_hpp */
