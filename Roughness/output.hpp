#ifndef output_hpp
#define output_hpp

#include <stdio.h>
#include <cstring>

#include <fstream>
#include <vector>

#include "parameters.hpp"
#include "functions.hpp"


using namespace std;

string getFullFileName(string filename);
void init(string, int, float *);
void outputvector(vector <float>, string);
void outputvector(vector <vector<float> >, string);
void outputatom(atom, string);

class OutputAtomBuilder{
	public:
		OutputAtomBuilder();
		void setFilename(string fileName);
		void addAtom(atom a);
		void setBoundaries(float coords[3][2]);
		void writeFile();

	private:
		string atomFile;
		string fileName;
		int atomCount;
		float coords[3][2];
};

#endif /* output_hpp */