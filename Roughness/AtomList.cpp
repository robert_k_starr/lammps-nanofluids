//
// Created by Robert Starr on 4/18/20.
//

#include <string>
#include <cmath>
#include "AtomList.h"
#include "output.hpp"

AtomList::AtomList() {
    setWall(-1);
    setRadius(-1);
    setNeighborDistance(-1);
}

atom * AtomList::getAtom(int i) {
    return allAtoms[i];
}

void AtomList::addAtom(atom atomToAdd) {
    allAtoms.push_back(new atom(atomToAdd));
    size++;
}

void AtomList::hydroxylate() {
    int startSize = allAtoms.size();
    for (int i = 0; i < startSize; i++) {
        atom * temp = getAtom(i);
        if (shouldAddHydrogenAtom(*getAtom(i))) {
            atom * edgeSiliconNeighbor = getAtom(i)->getEdgeSiliconNeighbor();
            addAtom(newHydrogen(*temp, *temp->getEdgeSiliconNeighbor()));
            temp->set_charge(OXYGEN_CHARGE);
            edgeSiliconNeighbor->set_charge(SILICON_CHARGE);
        }
    }
}

bool AtomList::shouldAddHydrogenAtom(atom oxygenAtom) {
    return (canHydroxylate(oxygenAtom) && (getTotalEdgeSiliconNeighbors(oxygenAtom) == 1) &&
            (oxygenAtom.getEdgeSiliconNeighbor()->charge == 0)
            );
}

int AtomList::getTotalEdgeSiliconNeighbors(atom oxygenAtom) {
    return oxygenAtom.getNumberOfNeighborsOfType(EDGE_SILICON);
}
atom AtomList::newHydrogen(atom oxygenAtom, atom siliconAtom){
    // A*B = |A||B|cos(theta)
    // Randomly choose x and y
    // (A_x * x + A_y*y)/(A_z*cos(theta)) = z
    // Normalize to HYDROGEN_LENGTH
    float x, y, z;
    float oxygenXComponent, oxygenYComponent, oxygenZComponent;	// Oxygen vectors
    int rand_range = 10000;

    oxygenXComponent = oxygenAtom.coords[0] - siliconAtom.coords[0];
    oxygenYComponent = oxygenAtom.coords[1] - siliconAtom.coords[1];
    oxygenZComponent = oxygenAtom.coords[2] - siliconAtom.coords[2];

    x = (rand() % rand_range); // Come up with method for randomizing +/-
    y = (rand() % rand_range);
    z = (oxygenXComponent * x + oxygenYComponent * y) / (oxygenZComponent * cos(HYDROGEN_ANGLE));	// Double check that angle should be
    // in degrees, not radians

    float length = sqrt(pow(x,2) + pow(y,2) + pow(z,2));

    x = siliconAtom.coords[0] + x * HYDROGEN_LENGTH / length;
    y = siliconAtom.coords[1] + y * HYDROGEN_LENGTH / length;
    z = siliconAtom.coords[2] + z * HYDROGEN_LENGTH / length;

    return atom(-1, x, y, z, EDGE_HYDROGEN, HYDROGEN_CHARGE);
    //int atomid, float a, float b, float c, int atomtype
}

bool AtomList::canHydroxylate(atom oxygenAtom){
    if (!oxygenAtom.is_deleted() && oxygenAtom.type == EDGE_OXYGEN) {
        return true;
    }
    return false;
}

void AtomList::setWall(float wallStart) {
    this->wallStart = wallStart;
}

void AtomList::setRadius(float radius) {
    this->radius = radius;
}

void AtomList::importFromFile(string lammpsOutputFile) {
     allAtoms = import(lammpsOutputFile);
     size = allAtoms.size();
     get_boundaries(lammpsOutputFile, minAndMax);
     setNeighbors();
}

void AtomList::sortByX() {
    std::sort(
            allAtoms.begin(),
            allAtoms.end(),
            [this](atom * one, atom * two){
                return sortByX(*one, *two);
            }
    );
}
void AtomList::setNeighbors() {
    std::map<std::tuple<int, int, int>, vector<atom*>> locationMap = establishLocationMap();

    for (int i = 0; i < allAtoms.size(); i++) {
//        This is very wrong.
        std::tuple<int, int, int> atomBox = determineBox(i);
        vector<std::tuple<int, int, int> > neighboringBoxes = findNeighboringBoxes(atomBox);
        for (int j = 0; j < neighboringBoxes.size(); j++) {
            if (locationMap.find(neighboringBoxes[j]) == locationMap.end()) continue;
            for (int k = 0; k < locationMap[neighboringBoxes[j]].size(); k++) {
                atom *testAtom = locationMap[neighboringBoxes[j]][k];
                if (shouldAddNeighbors(allAtoms[i], testAtom)) {
                    addNeighbors(allAtoms[i], testAtom);
                }

            }
        }
    }
}

void AtomList::findClosestDistance() {
    float minDistance = getAtom(0)->distance(*getAtom(1));

    for (int i = 0; i < allAtoms.size(); i++) {
        for (int j = i + 1; j < allAtoms.size(); j++)
            if (getAtom(i)->distance(*getAtom(j)) < minDistance) {
                minDistance = getAtom(i)->distance(*getAtom(j));
            }
    }
    cout << "Min Distance is:\t" << minDistance << "\n";
}

bool AtomList::sortByX(atom atom1, atom atom2) {
    return atom1.coords[0] < atom2.coords[0];
}

void AtomList::setNeighborDistance(float neighborDistance) {
    this->neighborDistance = neighborDistance;
}

float AtomList::averageNumberOfNeighbors() {
    float totalNeighbors = 0;
    for (int i=0; i<allAtoms.size(); i++) totalNeighbors += getAtom(i)->getNumberOfNeighbors();

    return totalNeighbors/allAtoms.size();
}

bool AtomList::shouldAddNeighbors(int i, int j) {
    return shouldAddNeighbors(getAtom(i), getAtom(j));
}

bool AtomList::shouldAddNeighbors(atom *atom1, atom*atom2) {
    return (atom1->distance(*atom2) < neighborDistance);
}

void AtomList::addNeighbors(int i, int j) {
    getAtom(i)->addNeighbor(*getAtom(j));
    getAtom(j)->addNeighbor(*getAtom(i));
}

void AtomList::addNeighbors(atom *atom1, atom *atom2) {
    atom1->addNeighbor(*atom2);
    atom2->addNeighbor(*atom1);
}

std::map<std::tuple<int, int, int>, vector<atom*>> AtomList::establishLocationMap() {
    std::map<std::tuple<int, int, int>, vector<atom*>> locationMap;

    for (int i = 0; i < allAtoms.size(); i++) {
        std::tuple<int, int, int> box = determineBox(i);
        if (locationMap.find(box) == locationMap.end()) {
            vector<atom*> newVector;
            newVector.push_back(getAtom(i));
            locationMap[box] = newVector;
        }
        else {
            locationMap[box].push_back(getAtom(i));
        }
    }

    return locationMap;
}

std::tuple<int, int, int> AtomList::determineBox(int atomNumber){
    int boxes[3];
    for (int i = 0; i < 3; i++) {
        boxes[i] = floor((getAtom(atomNumber)->coords[i])/(this->getNeighborDistance()));
    }
    return std::make_tuple(boxes[0], boxes[1], boxes[2]);
}

float AtomList::getNeighborDistance() {
    return neighborDistance;
}

vector<std::tuple<int, int, int>> AtomList::findNeighboringBoxes(tuple<int, int, int> boxTuple) {
    vector<std::tuple<int, int, int>> neighborBoxVector;
    for (int i = -1; i <= 1; i++) {
        neighborBoxVector.push_back(std::make_tuple(get<0>(boxTuple) + i, get<1>(boxTuple), get<2>(boxTuple)));
        neighborBoxVector.push_back(std::make_tuple(get<0>(boxTuple), get<1>(boxTuple) + i, get<2>(boxTuple)));
        neighborBoxVector.push_back(std::make_tuple(get<0>(boxTuple), get<1>(boxTuple), get<2>(boxTuple) + i));
    }
    return neighborBoxVector;
}

int AtomList::getSize() {
    return size;
}

void AtomList::recountSize() {
    int count = 0;
    for (int i = 0; i < allAtoms.size(); i++) {
        if (!getAtom(i)->is_deleted()) count++;
    }
}

void AtomList::removeCenter() {
    for (int i = 0; i < allAtoms.size(); i++) {
        atom *currentAtom = getAtom(i);
        if (shouldDeleteAtom(currentAtom)) {
            deleteAtom(currentAtom);
        }
        makeEdgeParticle(currentAtom);
    }
}

void AtomList::deleteAtom(atom *atomToDelete) {
    atomToDelete->delete_atom();
    size--;
}

bool AtomList::shouldDeleteAtom(atom *atomToCheck) {
    return (atomToCheck->coords[0] < wallStart) || (atomToCheck->distancefromcenter() < radius);
}

void AtomList::addRoughness() {

}

void AtomList::reID() {
    int currentID = 0;
    for (int i = 0; i < allAtoms.size(); i++) {
        if (getAtom(i)->is_deleted()) continue;
        getAtom(i)->set_id(currentID++);
    }
}

void AtomList::outputFile() {
    OutputAtomBuilder outputAtomBuilder;
    reID();
    outputAtomBuilder.setFilename(outputFileName);
    outputAtomBuilder.setBoundaries(minAndMax);
    int deletedCount = 0;
    float totalCharge = 0;
    for (int i = 0; i < allAtoms.size(); i++) {
        if (!(i % 1000)) printf("%i/%lu\n", i, allAtoms.size());
        if (getAtom(i)->is_deleted()) {
            deletedCount++;
            continue;
        }
        outputAtomBuilder.addAtom(*getAtom(i));
        float charge = getAtom(i)->getCharge();

        totalCharge += getAtom(i)->getCharge();
    }

    printf("%i deleted atoms.\n", deletedCount);
    printf("%f total charge.\n", totalCharge);

    outputAtomBuilder.writeFile();
}

void AtomList::setOutputFile(string fileName) {
    outputFileName = fileName;
}

void AtomList::setEdgeSize(float edgeThickness) {
    this->edgeThickness = edgeThickness;
}

int AtomList::countType(int type) {
    int count = 0;
    for (int i = 0; i < allAtoms.size(); i++) {
        if (getAtom(i)->is_deleted()) continue;
        if (getAtom(i)->getType() == type) count++;
    }
    return count;
}

void AtomList::makeEdgeParticle(atom *atom) {
    if (atom->distancefromcenter() < radius + edgeThickness) {
        switch (atom->getType()) {
            case SILICON:
                atom->set_type(EDGE_SILICON);
                break;
            case SILICA_OXYGEN:
                atom->set_type(EDGE_OXYGEN);
                break;
        }
    }
}
