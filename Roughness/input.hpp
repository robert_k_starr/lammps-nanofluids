#ifndef input_hpp
#define input_hpp

#include <stdio.h>
#include <cstring>
#include <fstream>
#include <vector>
#include <iostream>

#include "functions.hpp"

using namespace std;

vector<atom *> import(string);
float getvariable(string, string);
void get_boundaries(string, float[][2]);
#endif /* input_hpp */