//
// Created by Robert Starr on 4/18/20.
//

#ifndef ROUGHNESS_ATOMLIST_H
#define ROUGHNESS_ATOMLIST_H


#include <vector>
#include <map>
#include "functions.hpp"
#include "input.hpp"

class AtomList {
public:
    AtomList();
    void addAtom(atom);
    void hydroxylate();
    void setWall(float);
    void setRadius(float);
    void setNeighborDistance(float);
    float getNeighborDistance();
    void importFromFile(string);
    atom * getAtom(int);
    float averageNumberOfNeighbors();
    int getSize();
    void setOutputFile(string fileName);
    void removeCenter();
    void addRoughness();
    void outputFile();
    int countType(int type);
    void setEdgeSize(float edgeThickness);
    void recountSize();

private:
    std::vector<atom *> allAtoms;
    string outputFileName;
    int size;
    float wallStart, radius, neighborDistance, edgeThickness;
    float minAndMax[3][2];
    void setNeighbors();
    bool sortByX(atom, atom);
    bool shouldAddNeighbors(int i, int j);
    void addNeighbors(int i, int j);
    float distance(int i, int j);
    std::map<std::tuple<int, int, int>, vector<atom*>> establishLocationMap();
    void findClosestDistance();
    void sortByX();
    tuple<int, int, int> determineBox(int i);
    vector<std::tuple<int, int, int>> findNeighboringBoxes(tuple<int, int, int> tuple);
    void addNeighbors(atom *atom1, atom *atom2);
    bool shouldAddNeighbors(atom *atom1, atom *atom2);
    void deleteAtom(atom *atomToDelete);
    bool shouldDeleteAtom(atom *atomToCheck);
    static atom newHydrogen(atom oxygenAtom, atom siliconAtom);

    void makeEdgeParticle(atom *atom);

    bool shouldAddHydrogenAtom(atom oxygenAtom);

    int getTotalEdgeSiliconNeighbors(atom oxygenAtom);

    bool canHydroxylate(atom oxygenAtom);

    void reID();
};


#endif //ROUGHNESS_ATOMLIST_H
