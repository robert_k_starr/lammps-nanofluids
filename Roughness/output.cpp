#include <iostream>
#include <stdio.h>

#include "output.hpp"

using namespace std;

string getFullFileName(string filename) {
    string baseDirectory = "/Users/rstarr/Documents/UT Stuff/Nanoflow/";

    return baseDirectory + filename;
}
void init(string filename, int atoms, float coords[3][2]){
	ofstream file;
	file.open(filename,ios::trunc);
	file.close();
	file.open(filename, ios::app);
	file << "LAMMPS data file\n\n";
	file << atoms << " atoms\n";
	file << "8 atom types\n";
	file << "0 bonds\n";
	file << "3 bond types\n";
	file << "0 angles\n";
	file << "1 angle types\n\n";
	
	for (int i = 0; i < 3; i++){
		string direction = "xyz";
 		for (int j = 0; j < 2; j++){
 			file << coords[i][j] << " ";
 		}
		file << direction[i] << "lo " << direction[i] << "hi\n";
	}

	file << "\nMasses\n\n"; 
	file << "1 28.055\n";
	file << "2 15.9994\n";
	file << "3 15.9994\n";
	file << "4 1.008\n";
	file << "5 1.008\n";
	file << "6 15.9994\n";
	file << "7 28.055\n";
	file << "8 1000\n\n";
	
	file << "Atoms\n\n";

	file.close();
}

void outputvector(vector<float> values, string filename){
	ofstream file;
	int size = values.size();
	file.open(filename,ios::trunc);
	file.close();
	for (int i = 0; i < size; i++){
		file.open(filename, ios::app);
		file << values[i]<< "\n";
		file.close();
	}
}

void outputvector(vector<vector<float> > values, string filename){
	ofstream file;
	int size = values.size();
	file.open(filename,ios::trunc);
	file.close();
	for (int i = 0; i < size; i++){
		int vectorsize = values[i].size();
		for (int j = 0; j < vectorsize; j++){
			file.open(filename, ios::app);
			file << values[i][j]<< "\t";
			file.close();
		}
		file.open(filename,ios::app);
		file << "\n";
		file.close();
	}
}

OutputAtomBuilder::OutputAtomBuilder(){
	atomFile = "";
	atomCount = 0;
}

void OutputAtomBuilder::setFilename(string fileName) {
	this->fileName = fileName;
}

void OutputAtomBuilder::addAtom(atom a){
	atomFile += a.output() + "\n";
	atomCount++;
}

void OutputAtomBuilder::setBoundaries(float coords[3][2]){
	for (int i = 0; i < 3; i++) {
	    for (int j = 0; j < 2; j++) this->coords[i][j] = coords[i][j];
	}
}

void OutputAtomBuilder::writeFile(){
	init(fileName, atomCount, coords);
	ofstream file;
	file.open(fileName,ios::app);
	file << atomFile;
	file.close();
}
	

void outputatom(atom a, string filename){
	ofstream file;
	file.open(filename,ios::app);
	file << a.output() << "\n";
	file.close();
}