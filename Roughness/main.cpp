#include <iostream>
#include <cstring>
#include <cmath>
#include <stdlib.h>
#include <time.h>

#include "main.hpp"
#include "functions.hpp"
#include "input.hpp"
#include "parameters.hpp"
#include "output.hpp"
#include "AtomList.h"

using namespace std;

int main() {
    clock_t start = clock();

    srand(time(NULL));
    string filename = getFullFileName("silica.txt");
    AtomList atomList;
    atomList.setNeighborDistance(NEIGHBOR_DISTANCE);
    atomList.importFromFile(filename);
    innerradius = getvariable(getFullFileName("variables.txt"), "INNERRADIUS");
    wallstart	= getvariable(getFullFileName("variables.txt"), "WALLSTART");

    atomList.setWall(wallstart);
    atomList.setRadius(innerradius);
    atomList.setEdgeSize(EDGE_THICKNESS);

    std::printf("%i atoms. About to remove center.\n", atomList.getSize());
	atomList.removeCenter();
    std::printf("%i atoms after center removed.\n", atomList.getSize());

//    atomList.addRoughness();

    atomList.hydroxylate();
    std::printf("%i atoms after hydroxylation.\n", atomList.getSize());
    printf("%i atoms of type EDGE_SILICON\n", atomList.countType(EDGE_SILICON));
    printf("%i atoms of type EDGE_OXYGEN\n", atomList.countType(EDGE_OXYGEN));
    printf("%i atoms of type SILICON\n", atomList.countType(SILICON));
    printf("%i atoms of type HYDROGEN\n", atomList.countType(EDGE_HYDROGEN));

    atomList.setOutputFile(getFullFileName("newsilica.txt"));

    atomList.outputFile();
    printf("\n\nFile output complete.\n\n");
    printf("%i atoms of type EDGE_SILICON\n", atomList.countType(EDGE_SILICON));
    printf("%i atoms of type EDGE_OXYGEN\n", atomList.countType(EDGE_OXYGEN));
    printf("%i atoms of type SILICON\n", atomList.countType(SILICON));
    printf("%i atoms of type HYDROGEN\n", atomList.countType(EDGE_HYDROGEN));

    clock_t stop = clock();
    double elapsed = (double) (stop - start) / CLOCKS_PER_SEC;
    printf("\nTime elapsed: %.5f seconds\n", elapsed);
    return 0;
}
