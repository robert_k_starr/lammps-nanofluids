#include "input.hpp"
#include "functions.hpp"


vector<atom *> import(string filename){
	ifstream file(filename);
	string line;
	
	int numberOfAtoms = 0;
	int i = 0;

	while(line != "Atoms # hybrid"){
	    if (line.find("atoms") != string::npos) {
	        numberOfAtoms = stoi(split(line)[0]);
	    }
		getline(file,line);
	}

    vector<atom *> list;
	getline(file,line);
	getline(file,line);
	while(line != ""){
		list.push_back(new atom(line)); // Add the atom to our list.
		getline(file,line);
	}
	return list;
}

float getvariable(string filename, string variable){
	ifstream file(filename);
	string line;
	
	while (getline(file,line)){
		if (!line.find("variable " + variable + " ")){
			return stof(split(line)[3]);
		}
	}
	
	return 0;
}

void get_boundaries(string filename, float coords[][2]){
	ifstream file(filename);
	string line;
	size_t found;
	while(getline(file,line)){
		found = line.find("xlo xhi");
		if(found != string::npos){
			coords[0][1] = stof(split(line)[0]);
			coords[0][1] = stof(split(line)[1]);
		}
		found = line.find("ylo yhi");
		if(found != string::npos){
			coords[1][0] = stof(split(line)[0]);
			coords[1][1] = stof(split(line)[1]);
		}
		found = line.find("zlo zhi");
		if(found != string::npos){
			coords[2][0] = stof(split(line)[0]);
			coords[2][1] = stof(split(line)[1]);
		}
		found = line.find("Masses");
		if (found!= string::npos) {
		    break;
		}
	}	
}