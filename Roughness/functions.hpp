#ifndef functions_hpp
#define functions_hpp

#include <stdio.h>
#include <cstring>
#include <vector>

//#include "parameters.hpp"

using namespace std;

class atom{
    bool deleteatom;
    atom *neighbors[30];

	public:
		int type, id;
		float coords[3], charge;
		atom *parent[3];
		atom *left[3];
		atom *right[3];
		atom();
		atom(int, float, float, float, int);
		atom(int, float, float, float, int, float);
		atom(string);
		void location();
		void delete_atom();
		void undelete_atom();
		void set_parent(atom&, int);
		void set_daughter(atom&, int);
		void set_charge(float);
		float getCharge();
		void addNeighbor(atom &newNeighbor);
		void reveal_parent(int);
		float distancefromcenter();
		float distance(atom);
		void set_type(int);
		string output();
		void set_id(int);
		bool is_deleted();
		int getNumberOfNeighbors();
		int getNumberOfNeighborsOfType(int);
        atom* getEdgeSiliconNeighbor();
        int getType();
private:
    int numberOfNeighbors;
};

vector<string> split(string);
void add_roughness(atom, float);
int distance(atom, atom);
void undelete_daughters(atom&, atom&, float);
vector<atom *> return_surrounding_atoms(atom, float);
vector<atom *> return_surrounding_daughters(atom, atom, float);
atom new_hydrogen(atom, atom);
atom new_hydrogen(atom);
int hydroxylate(vector<atom> &, float, float);
void re_id_atoms(vector<atom> &);

#endif /* functions_hpp */