import os, math

def extract(line):
	data = []
	temp = ""
	for a in line:
		if (a == " "):
			data.append(temp)
			temp = ""
			continue
		if (a == "\n"):
			break
		if (not a.isdigit()) and (not a == "."):
			return False
		temp = temp + a
	if len(data) != 3:
		return False
	if data[0] == 4:
		return False
	return float(data[2])

def average(a):
	sum = 0
	for value in a:
		sum = sum + value
	return float(sum)/len(a)

def stddev(a):
	avg = average(a)
	sumsq = 0
	for value in a:
		sumsq = sumsq + (value - avg)**2
	return math.sqrt((sumsq)/(len(a)-1))

for files in os.listdir("MiddleFrames"):
	atoms = []
	openfile = open("MiddleFrames/" + files, 'r')
	time = int(files[:-4])/10
	coords = [time, 0]
	for line in openfile:
		if extract(line):
			atoms.append(extract(line))
	for i in range(0, len(atoms)-1):
		for j in range(i + 1, len(atoms)):
			if atoms[i] > atoms[j]:
				temp = atoms[i]
				atoms[i] = atoms[j]
				atoms[j] = temp
	for i in range(1, len(atoms) + 1):
		j = len(atoms) - i
		avg = average(atoms)
		std = stddev(atoms)
		if atoms[j] < avg + 2*std:
			coords[1] = atoms[j]
			break
	print coords