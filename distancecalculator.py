import time, math

def distance(atom1, atom2):
	square = 0
	for value in range(0, 3):
		square = square + pow(atom1[value] - atom2[value], 2)
	return math.sqrt(square)

f = open('fun.txt', 'r')

atoms = []

for line in f:
	spaces = []
	coords = []
	length = len(line)
	for value in range(0, length):
		if line[value] == " ":
			spaces.append(value)
	for value in range(0, 3):
		coords.append(float(line[spaces[value + 1] + 1: spaces[value + 2]]))
	atoms.append(coords)

print len(atoms)
	
for atom1 in range(0, len(atoms)):
	#print " "
	#print atom1
	for atom2 in range(atom1 + 1, len(atoms)):
		x = distance(atoms[atom1], atoms[atom2])
		if x == 0:
			print str(atom1 + 1) + " = " + str(atom2 + 1)
		if x < .433:
			print "\n\n\nERROR, VALUE LESS THAN SQRT(3)/4\n\n\n"
		#print x