#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>

#include "output.hpp"
#include "parameters.hpp"
#include "particles.hpp"

using namespace std;

void printfile(string filename, string text){
	ofstream file;
	
	file.open(filename, ios::app);
	file << text;
	file.close();
}

void outputtext(string &a, string b){
	a += b;
}

void outputtext(string &file, double number){
	string temp;
	ostringstream convert;
	convert << number;
	
	temp = convert.str();
	
	outputtext(file, temp);
}

void outputtext(string &file, double *number, int n){
	int i;
	for (i = 0; i < n; i++){
		outputtext(file, number[i]);
		outputtext(file, " ");
	}
	outputtext(file, "\n");
}
void outputtext(string &file, int number){
	outputtext(file, (double) number);
}

void cleanfile(string filename){
	ofstream file;
	file.open(filename);
	file.close();
}

void combinefile(string file1, string file2){
	ifstream infile(file1);
	string line;
	while (getline(infile, line)) {
		outputtext(file2, line);
		outputtext(file2, "\n");
	}
}

void recvariable(string file, string name, double value){
	string temp;
	outputtext(temp,"variable ");
	outputtext(temp, name);
	outputtext(temp," equal ");
	outputtext(temp, value);
	outputtext(temp, "\n");
	printfile(file, temp);
}

void generateinput(){	
	double negradius = ymin, posradius = ymax;
	double radius = (posradius - negradius)/2;

	recvariable(variablefile, "NEGRADIUS", negradius);
	recvariable(variablefile, "POSRADIUS", posradius);
	recvariable(variablefile, "HEIGHT", xmax);
	
	recvariable(variablefile, "WALLSTART", WALLSTART);
	
	recvariable(variablefile, "RADIUS", (posradius - negradius)/2.0);
	recvariable(variablefile, "INNERRADIUS", innerradius);
	
	double temp[6] 		= {xmin, xmax, THICK + SIDIST - .01, DIAMETER + THICK - SIDIST + .01, THICK + 2.0, THICK + DEPTH - 2.0};
	double reservoir[6]	= {0, WALLSTART - 1.0, 0, ymax, zmin, zmax};
	double zaxis[6]		= {0, xmax, 0, ymax, (zmax - WATERDIST)/2.0, (zmax + WATERDIST)/2.0};
	
// 	outputtext(variablefile, "\nregion temp1 block ");
// 	outputtext(variablefile, temp, 6);
// 	
// 	outputtext(variablefile, "region temp2 block ");
// 	outputtext(variablefile, reservoir, 6);
// 	
// 	outputtext(variablefile, "region movers union 2 temp1 temp2\n");
// 	
// 	outputtext(variablefile, "region zaxis block ");
// 	outputtext(variablefile, zaxis, 6);
// 	
// 	outputtext(variablefile, "region display intersect 2 movers zaxis\n");
}