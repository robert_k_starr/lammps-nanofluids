#include <iostream>
#include <cstring>
#include <math.h>
#include "particles.hpp"
#include "functions.hpp"
#include "output.hpp"

using namespace std;

const double pi = atan(1)*4;

long int atom_count = 0;
long int bonds = 0;
long int angles = 0;

atom::atom(double x, double y, double z) {
	coords[0] = x;
	coords[1] = y;
	coords[2] = z;
	if (checkcoords(coords)) initiate();
}

atom::atom(double *a, int n = 3){
	copy(coords, a, n);
	if (checkcoords(coords)) initiate();
}

atom::atom(){}

void atom::initiate(){
	atom_count = atom_count + 1;
	id = atom_count;
}

void atom::printatom(){
	double info[8] = {(double) id, (double) type, coords[0], coords[1], coords[2], (double) molecule, charge, (double) molecule};
	if (checkcoords(coords)) outputtext(atomfile, info, 8);
}

void atom::setcharge(double value) {
	charge = value;
}

void atom::setmass(double value) {
	mass = value;
}

void atom::returncoords(double *newcoords){
	for (int i = 0; i < 3; i++) newcoords[i] = coords[i];
}

void atom::settype(int value) {
	type = value;
	if (value == HYDROGEN)		name = "Hydrogen";
	else if (value == SILICON)	name = "Silicon";
	else if (value == OXYGEN)	name = "Oxygen";
	else 						name = "NO NAME";
	
}

void atom::setmolecule(int value){
	molecule = value;
}

void atom::displaycoords(){
	printcoords(this->coords, 3);
}

int atom::returnid(){
	return id;
}

atom oxygen(double x, double y, double z, int molecule){
	atom temp(x, y, z);
	temp.settype(OXYGEN);
	temp.setcharge(-1.0484);
	temp.setmass(15.9994);
	temp.setmolecule(molecule);

	temp.printatom();
	return temp;
}

atom oxygen(double *a, int molecule){
	return oxygen(a[0], a[1], a[2], molecule);
}

atom hydrogen(double x, double y, double z, int molecule){
	atom temp(x, y, z);
	temp.settype(HYDROGEN);
	temp.setcharge(.5242);
	temp.setmass(1.008);
	temp.setmolecule(molecule);

	temp.printatom();
	return temp;
}

atom hydrogen(double *a, int molecule){
	return hydrogen(a[0], a[1], a[2], molecule);
}

atom silicon(double x, double y, double z){
	atom temp(x, y, z);
	temp.settype(SILICON);
	temp.setcharge(.9);
	temp.setmass(0.0);			// Figure out Silicon Mass

	temp.printatom();
	return temp;
}

atom silicon(double *a){
	return silicon(a[0], a[1], a[2]);
}

void atom::display(){
	cout << "\nType: " + this->name + "\n";
 	printf("Mass: %f\n", this->mass);
 	printf("Charge: %f\n", this->charge);
 	printf("ID: %i\n\n", this->id);
}

water::water(double x, double y, double z, int molecule){
	double temp[3] = {x, y, z};
	initiate(temp, molecule);
}

water::water(double *a, int molecule){
	initiate(a, molecule);
}

void water::initiate(double *a, int molecule){
	double bondangle = ((double) (rand() % 360))*pi/180.0;
	double bondlength = HBONDDIST;
	double b[3] = {bondlength, bondangle, 0};
	double temp[3];
	
	ox = oxygen(a, molecule);
	cart(b);
	addcoords(b, a, temp);
	h1 = hydrogen(temp, molecule);
	
	bondangle = bondangle + HBONDANGLE*pi/180.0;
	polar(b);
	b[1] = bondangle;
	cart(b);
	
	addcoords(b, a, temp);
	h2 = hydrogen(temp, molecule);
	
	bond(ox, h1);
	bond(ox, h2);
	
	angle(h1, ox, h2);
}

void water::displaycoords(){
	printf("\nOxygen:\n");
	ox.displaycoords();
	printf("Hydrogen 1:\n");
	h1.displaycoords();
	printf("Hydrogen 2:\n");
	h2.displaycoords();
}

void water::displayatoms(){
	printf("\nOxygen:\n");
	ox.display();
	printf("Hydrogen 1:\n");
	h1.display();
	printf("Hydrogen 2:\n");
	h2.display();
}

void silicaleft(double * coords){
	atom temp = silicon(coords);
	
	double oxy[3];
	int i;
	for  (i = 0; i < 3; i++) oxy[i] = coords[i];
	
	oxy[0] = oxy[0] + SIDIST/2;
	oxygen(oxy, 0);
}

void silicaright(double *coords){
	atom temp = silicon(coords);
	double oxy[3];
	int i;
	for  (i = 0; i < 3; i++) oxy[i] = coords[i];
	oxy[0] = oxy[0] + SIDIST/4;
	oxy[1] = oxy[1] + SIDIST*sqrt(3)/4;
	oxygen(oxy, 0);
	oxy[1] = oxy[1] - SIDIST*sqrt(3)/2;
	oxygen(oxy, 0);
}


void bond(atom a, atom b){
	double temp[4] = {(double) bonds, (double) 1, (double) a.returnid(), (double) b.returnid()};

	if ((checkcoords(a)) and (checkcoords(b))){
		bonds++;
		outputtext(bondfile, temp, 4);
	}
}

void angle(atom a, atom b, atom c){

	double temp[5] = {(double) angles, (double) 1, (double) a.returnid(), 
									(double) b.returnid(), (double) c.returnid()};

	if ((checkcoords(a) and checkcoords(b) and checkcoords(c))){																	
		angles++;	
		outputtext(anglefile, temp, 5);
	}
}

int numberatoms(){
	return atom_count;
}

int numberbonds(){
	return bonds;
}

int numberangles(){
	return angles;
}