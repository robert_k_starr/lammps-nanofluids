#ifndef parameters_hpp
#define parameters_hpp

#include <stdio.h>

#define DIAMETER 30	//	Diameter of inner cylinder
#define THICK 10		  //	Thickness of outer cylinder
#define LENGTH 200	//	"Height" of cylinder -- corresponds to length in x-direction
#define DEPTH 10
#define START 50     // Where the wall starts
#define WATERSTART 10

#define BUFFER 1

extern double xmin, xmax, ymin, ymax, zmin, zmax, innerradius;

#endif /* parameters_hpp */
