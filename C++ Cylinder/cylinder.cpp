#include <iostream>
#include <cstring>

#include "cylinder.hpp"
#include "particles.hpp"
#include "parameters.hpp"
#include "output.hpp"
#include "functions.hpp"

using namespace std;

int main(){
	
	cleanfile(datafile);    // This can be simplified a lot by searching for *.txt and deleting them
	cleanfile(variablefile);
// 	cleanfile("atoms.txt");
// 	cleanfile("bonds.txt");
// 	cleanfile("angles.txt");
	
	double a[3], b[3];
	
// 	a[0] = WALLSTART/2;
// 	a[1] = ymin;
// 	a[2] = zmin;
// 	
// 	b[0] = WALLSTART - SIDIST;
// 	b[1] = ymax;
// 	b[2] = zmax;
// 	
// 	hexfill(a, b, 3, WATER, WATERDIST);
	
	int i = 0;
	
	a[0] = WATERSTART; // - SIDIST + WATERDIST;
	a[1] = ymin - SIDIST;
	a[2] = zmin - SIDIST;
	
	b[0] = WALLSTART + 10;
	b[1] = ymax + SIDIST;
	b[2] = zmax + SIDIST;
	
	hexfill(a, b, 3, WATER, WATERDIST, radius - WATERDIST, 0);

	initialize(finaldata);
	generateinput();
	
	outputtext(finaldata, "Atoms\n\n");
	outputtext(finaldata, atomfile);
	
	outputtext(finaldata, "\nBonds\n\n");
	outputtext(finaldata, bondfile);
	
	outputtext(finaldata, "\nAngles\n\n");
	outputtext(finaldata, anglefile);
	
	outputtext(finaldata, "\n");
	
	printfile(datafile, finaldata);	
}