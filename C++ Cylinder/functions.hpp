#ifndef functions_hpp
#define functions_hpp

#include "particles.hpp"

double distance(double *, double *, int);
double distance(double *, double *);

void midpoint(double *, double *, double *, int);
void midpoint(double *, double *, double *);

void cart(double *);
void cart(double, double, double);
void polar(double *);
void polar(double, double, double);

void printcoords(double *, int);
void printcoords(double *);

void copy(double *, double *, int);
void copy(double *, double *);

void addcoords(double *, double *, double *, int);
void addcoords(double *, double *, double *);

void hexfill(double *, double *, int, int, double, int);
void hexfill(double *, double *, int, int, double, double, int);

bool checkcoords(double *);
bool checkcoords(atom);

void initialize(string &);

#endif /* functions_hpp */