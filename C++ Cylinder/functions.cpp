#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "functions.hpp"
#include "particles.hpp"
#include "output.hpp"
#include "parameters.hpp"

using namespace std;

double distance(double *a, double *b, int n){
	int i;

	double sum = 0;
	
	for (i = 0; i < n; i++) sum = sum + pow(a[i] - b[i], 2);
	
	return sqrt(sum);	
}

double distance(double *a, double *b){
	return distance(a, b, 3);
}

void midpoint(double *a, double *b, double *c, int n){			// Plugs midpoint into array "C"
	int i;
	
	for (i = 0; i < n; i++) c[i] = (a[i] + b[i])/2;

}

void midpoint(double *a, double *b, double *c){
	midpoint(a, b, c, 3);
}

void cart(double *a){
	double temp[3];
	
	temp[0] = a[0]*cos(a[1]);
	temp[1] = a[0]*sin(a[1]);
	temp[2] = a[2];
	
	copy(a, temp, 3);
}

void cart(double r, double theta, double z){
	double temp[3] = {r, theta, z};
	cart(temp);
}

void polar(double *a){
	double temp[3];
	
	temp[0] = sqrt(pow(a[0],2) + pow(a[1],2));
	temp[1] = atan(a[1]/a[0]);
	temp[2] = a[2];
	
	copy(a, temp);
}

void polar(double x, double y, double z){
	double temp[3] = {x, y, z};
	
	polar(temp);
}

void printcoords(double *a, int n){
	int i;
	
	printf("(");
	
	for (i = 0; i < n; i++) printf("%f, ", a[i]);
	
	printf("\b\b)\n");
}

void printcoords(double *a){
	printcoords(a, 3);
}

void copy(double *a, double *b, int n){
	int i;
	
	for (i = 0; i < n; i++) a[i] = b[i];
}

void copy(double *a, double *b){
	copy(a, b, 3);
}

void addcoords(double *a, double *b, double *c, int n){
	int i;
	for (i = 0; i < n; i++) c[i] = a[i] + b[i];
}

void addcoords(double *a, double *b, double *c){
	addcoords(a, b, c, 3);
}

void hexfill(double *start, double *end, int dimensions, int type, double spacing, int molecule){
	double count = 0;
	
	double xmin = min(start[0], end[0]);
	double xmax = max(start[0], end[0]);
	
	double ymin = min(start[1], end[1]);
	double ymax = max(start[1], end[1]);
	
	double zmin = min(start[2], end[2]);
	double zmax = max(start[2], end[2]);
	
	bool shift = false;
	
	double coord[3] = {xmin, ymin, zmin};
	
	while (coord[2] <= zmax){
		while (coord[1] <= ymax){
			while (coord[0] <= xmax){
				if 		(type == OXYGEN) 	oxygen(coord, 0);
				else if	(type == HYDROGEN) 	hydrogen(coord, 0);
				else if	(type == SILICON) 	silicon(coord);
				else if	(type == WATER)		new water(coord, ++molecule);
				else if (type == SILICA){
					if ((not shift) and ((int) round((coord[0] - xmin)/spacing)%3 == 1)){
						coord[0] = coord[0] + spacing;
						silicaleft(coord);
					}
					else if (shift and ((int) round((coord[0] - spacing/2 - xmin)/spacing)%3 == 2)){
						coord[0] = coord[0] + spacing;
						silicaleft(coord);
					}
					else silicaright(coord);
					
				}
				// Need to set up Silica Spacing... And then figure out how to bond them
				coord[0] = coord[0] + spacing;
			}
			coord[0] = xmin;
			
			if (shift) shift = false;
			else{
				shift = true;
				coord[0] = coord[0] + spacing/2;
			}
			coord[1] = coord[1] + spacing*sqrt(3)/2.0;
		}

		coord[1] = ymin;
		coord[2] = coord[2] + spacing;
	}
}

void hexfill(double *start, double *end, int dimensions, int type, double spacing, double radius, int molecule){
	double count = 0;
	
	double xmin = min(start[0], end[0]);
	double xmax = max(start[0], end[0]);
	
	double ymin = min(start[1], end[1]);
	double ymax = max(start[1], end[1]);
	
	double zmin = min(start[2], end[2]);
	double zmax = max(start[2], end[2]);
	
	bool shift = false;
	
	double coord[3] = {xmin, ymin, zmin};
	
	while (fabs(coord[2]) <= zmax){
		double tempymax = sqrt(pow(radius, 2) - pow(coord[2], 2));
		coord[1] = -tempymax;
		while (fabs(coord[1]) <= tempymax){
			while (coord[0] <= xmax){
				if 		(type == OXYGEN) 	oxygen(coord, 0);
				else if	(type == HYDROGEN) 	hydrogen(coord, 0);
				else if	(type == SILICON) 	silicon(coord);
				else if	(type == WATER)		new water(coord, ++molecule);
				else if (type == SILICA){
					if ((not shift) and ((int) round((coord[0] - xmin)/spacing)%3 == 1)){
						coord[0] = coord[0] + spacing;
						silicaleft(coord);
					}
					else if (shift and ((int) round((coord[0] - spacing/2 - xmin)/spacing)%3 == 2)){
						coord[0] = coord[0] + spacing;
						silicaleft(coord);
					}
					else silicaright(coord);
					
				}
				// Need to set up Silica Spacing... And then figure out how to bond them
				coord[0] = coord[0] + spacing;
			}
			coord[0] = xmin;
			
			if (shift) shift = false;
			else{
				shift = true;
				coord[0] = coord[0] + spacing/2;
			}
			coord[1] = coord[1] + spacing*sqrt(3)/2.0;
		}

		coord[1] = ymin;
		coord[2] = coord[2] + spacing;
	}
}

bool checkcoords(double *coords){
	if ((coords[0] < xmin) or (coords[0] > xmax)) return false;
	if ((coords[1] < ymin) or (coords[1] > ymax)) return false;
	if ((coords[2] < zmin) or (coords[2] > zmax)) return false;
	
	return true;
}

bool checkcoords(atom atom1){
	double n[3];
	atom1.returncoords(n);
	return checkcoords(n);
}
void initialize(string &file){
	outputtext(file, "LAMMPS Data File \n\n");
	outputtext(file, numberatoms());
	outputtext(file, " atoms\n");
	outputtext(file, numberbonds());
	outputtext(file, " bonds\n");
	outputtext(file, "\n");
	outputtext(file, numberangles());
	outputtext(file, " angles\n");
	
	outputtext(file, "4 atom types\n");
	outputtext(file, "3.0 bond types\n\n");
// 	outputtext(file, "1.0 angle types\n\n");
	
	outputtext(file, xmin - BUFFER);
	outputtext(file, " ");
	outputtext(file, xmax + BUFFER);
	outputtext(file, " xlo xhi\n");
	
	outputtext(file, ymin - BUFFER);
	outputtext(file, " ");
	outputtext(file, ymax + BUFFER);
	outputtext(file, " ylo yhi\n");
	
	outputtext(file, zmin - BUFFER);
	outputtext(file, " ");
	outputtext(file, zmax + BUFFER);
	outputtext(file, " zlo zhi\n\n");
}