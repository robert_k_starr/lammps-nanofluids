#ifndef output_hpp
#define output_hpp

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>

using namespace std;

void printfile(string, string);

void outputtext(string &, string);
void outputtext(string &, double);
void outputtext(string &, double *, int);
void outputtext(string &, int);

void cleanfile(string);
void combinefile(string, string);

void generateinput();

extern string datafile;
extern string variablefile;

extern double WALLSTART;

#endif /* output_hpp */

