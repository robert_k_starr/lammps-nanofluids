#ifndef particles_hpp
#define particles_hpp

#include <stdio.h>
#include <cstring>

using namespace std;

#define SILICON		1
#define OXYGEN		3
#define METHANE		3
#define HYDROGEN	4

#define WATER		-1
#define SILICA		-2

#define SIDIST		3.1
#define	WATERDIST	3.1
#define HBONDDIST	.9572

#define HBONDANGLE 104.52

class atom{
		int type, id, molecule;
		string name;
		double charge, mass;
		double coords[3];

	public:		
		atom(double, double, double);
		atom(double *, int);
		atom(double *);
		atom();
		void settype(int);
		void setcharge(double);
		void setmass(double);
		void setmolecule(int);
		void display();
		void printatom();
		void returncoords(double *);
		void displaycoords();
		void initiate();
		int returnid();
		double returnspace();

};


// Molecules

class water{
	double xpos, ypos, zpos;
	double bondangle;
	
	public:
		atom ox, h1, h2;
		water(double, double, double, int);
		water(double *, int);
		void initiate(double *, int);
		void displaycoords();
		void displayatoms();
};

// Functions

atom oxygen(double, double, double, int);
atom oxygen(double *, int);

atom silicon(double, double, double);
atom silicon(double *);

atom hydrogen(double, double, double, int);
atom hydrogen(double *, int);

void silicaright(double *);
void silicaleft(double *);

atom display();

void bond(atom, atom);
void angle(atom, atom, atom);

int numberatoms();
int numberbonds();
int numberangles();

extern string atomfile, bondfile, anglefile;

#endif /* particles_hpp */