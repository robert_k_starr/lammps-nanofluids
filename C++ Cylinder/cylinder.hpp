#ifndef cylinder_hpp
#define cylinder_hpp

#include <stdio.h>
#include <cstring>
#include "parameters.hpp"

using namespace std;

string datafile = "nanofile.txt";
string variablefile = "variables.txt";

double xmin = 0;
double xmax = LENGTH;
double ymin = -DIAMETER/2 - THICK;
double ymax = DIAMETER/2 + THICK;
double zmin = ymin;
double zmax = ymax;

double radius = (ymax - ymin)/2;
double innerradius = radius - THICK;

double WALLSTART 	= (double) START;
double WALLEND		= (double) LENGTH;

string finaldata, atomfile, anglefile, bondfile;

#endif /* cylinder_hpp */
