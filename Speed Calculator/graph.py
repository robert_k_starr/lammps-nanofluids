## Giving value of .045
## I calculate the constant should be .032

## Note: Be sure to run minimize.py first

import numpy as np
import matplotlib.pyplot as plt
import random
import math

# from minfunc import *

filename = "meniscii.txt"
alpha = 1e-9
delta = 1e-2
file = open(filename,'r')
length = 0

for line in file:
	length = length + 1

file = open(filename,'r')

minimumvalue = 0
outliersize = 2

x = []
y = []

i = -1

parameters = open('../variables.txt','r')

for line in parameters:
	if "WALLSTART" in line:
		minimumvalue = float(line.split()[3])*1.1
		
def swap(a, m, n):
	temp = a[m]
	a[m] = a[n]
	a[n] = temp
	return a

def orderarrays(a,b):
	if (len(a) == 0):
		return False
	if (len(a) == 0):
		return [a,b]
	x = random.randint(0,len(a)-1)
	a = swap(a,0,x)
	b = swap(b,0,x)
	

	j = len(a) - 1
	i = 1
	pivot = a[0]
	bvalue	= b[0]
	while (i <= j):
		if (a[i] > pivot):
			a = swap(a, i, j)
			b = swap(b, i, j)
			j = j - 1
		else:
			i = i + 1
	a = swap(a,0,j)
	b = swap(b,0,j)

	temp1 = orderarrays(a[:j],b[:j])
	if len(a) >= j + 2:
		temp2 = orderarrays(a[j + 1:],b[j+1:])
	else:
		temp2 = False
	
	if temp1:
		a = temp1[0] + [a[j]] 
		b = temp1[1] + [b[j]]
	if temp2:
		a = a[:j + 1] + temp2[0]
		b = b[:j + 1] + temp2[1]
	return [a, b]

# temp = (file.readline()).split()
# # consts = []
# # for value in temp:
# # 	consts.append(float(value))
# # 
# # temp = (file.readline()).split()
# # consts2 = []
# # for value in temp:
# # 	consts2.append(float(value))
xmin = 1e16
ymin = 1e16	
for line in file:
	#i = i + 1
	temp = line.split()
	# if (math.log(float(temp[0])) > 14.05):
	# 	continue
	# if (float(temp[1]) > minimumvalue):
	x.append((float(temp[0])))
	y.append((float(temp[1])))
	if (x[-1] < xmin):
		xmin = x[-1]
	if (y[-1] < ymin):
		ymin = y[-1]
newX = []
newY = []
for i in range(0, len(x)):
	if ((x[i] != xmin) and (y[i] != ymin)):
# 		if (math.log(x[i] - xmin) < 13.1):
# 			continue
		# if (math.log(x[i] - xmin) > 11.9):
		# 	continue
		newX.append(math.log(x[i] - xmin))
		newY.append(math.log(y[i] - ymin))

x = newX
y = newY

temp = orderarrays(x,y)
predy = []
sqrtgraph = []
# 
x = temp[0]
y = temp[1]
# 
# consts = minsq(temp, alpha, delta)

fig = plt.figure(1, figsize=(8,8))

m, b = np.polyfit(x, y, 1)

for i in x:
	predy.append(m*i + b)

print("y = " + str(m) + "x + " + str(b))

ax1 = fig.add_subplot(111)
# ax1.plot(x,y, 'rs')
ax1.plot(x,y,'rs', x, predy, 'b-', linewidth=3)
ax1.set_xlabel('t')
ax1.set_ylabel('x')
	
plt.savefig('test.png')
