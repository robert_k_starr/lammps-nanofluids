//
// Created by Robert Starr on 11/9/19.
//

#include <fstream>
#include <vector>
#include <iostream>
#include <dirent.h>
#include "input.h"

float get_variable_from_file(string variable, string filename) {
    string line = find_variable_line(variable, filename);

    return get_variable_from_line(variable, line);
}

vector<string> list_files(string path) {
    DIR *dir;
    struct dirent *ent;
    vector<string> files;
    const char *directory = path.c_str();
    if ((dir = opendir(directory)) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            string temp = ent->d_name;
            if (temp.compare(0, 3, "xyz") == 0) files.push_back(path + "/" + temp);
        }
        closedir(dir);
    }

    return files;
}

string find_variable_line(string variable, string filename) {
    string line;
    ifstream file(filename);

    while(getline(file, line)) {
        vector<string> split_line = split(line);
        if (!split_line[1].compare(variable)) return line;
    }

    return std::string();
}

vector<string> split(string a){
    return split(a, "\t");
}



float get_variable_from_line(string variable, string line) {
    return stof(split(line)[3]);
}

vector<string> split(string input_string, string break_characters) {
    vector<string> results;
    string temp;
    string delimiter = " " + break_characters;
    size_t pos = 0;
    while ((pos = input_string.find_first_of(delimiter)) != string::npos) {
        temp = input_string.substr(0, pos);
        results.push_back(temp);
        input_string.erase(0, pos + delimiter.length() - 1);
    }
    results.push_back(input_string);
    return results;
}


