//
// Created by Robert Starr on 11/9/19.
//

#include <string>
#include <fstream>
#include <iostream>
#include "functions.h"
#include "input.h"

vector<float> get_x_coordinates(string atom_file) {
    string line;
    ifstream file(atom_file);
    vector<float> x_vector;
    while(getline(file, line)) {
        if (!line.compare("ITEM: ATOMS id type x y z ")) break;
    }
    while(getline(file, line)) {
        if (get_type(line) == 3) x_vector.push_back(get_x(line));
    }
    return x_vector;
}

float get_type(string line){
    return stof(split(line, " ")[1]);
}

float get_x(string line) {
    return stof(split(line, " ")[2]);
}

int get_file_number(string filename) {
    string temp;
    int size = filename.length();
    for (int i = 0; i < size; i ++){
        if (isdigit(filename[i])) temp = temp + filename[i];
    }
    return stoi(temp);
}


