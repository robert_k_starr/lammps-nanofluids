//
// Created by Robert Starr on 11/9/19.
//

#ifndef NEW_SPEED_CALCULATOR_INPUT_H
#define NEW_SPEED_CALCULATOR_INPUT_H

#include <string>

using namespace std;

float get_variable_from_file(string variable, string filename);
vector<string> list_files(string path);
float get_variable_from_line(string variable, string line);
string find_variable_line(string variable, string filename);
vector<string> split(string a);
vector<string> split(string input_string, string delimiter);

#endif //NEW_SPEED_CALCULATOR_INPUT_H
