//
// Created by Robert Starr on 11/10/19.
//

#include <fstream>
#include "output.h"

void output_file(string filename, string text_to_write){
    ofstream file;

    file.open(filename,ios::trunc);
    file.close();

    file.open(filename, ios::app);
    file << text_to_write;
    file.close();
}
