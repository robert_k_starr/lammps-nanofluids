//
// Created by Robert Starr on 11/10/19.
//

#include <cmath>
#include <iostream>
#include "cylinder.h"

//    startx, finalx, number_of_bins should be elements of a class
void cylinder::setStartx(float startx) {
    cylinder::startx = startx;
}

void cylinder::setFinalx(float finalx) {
    cylinder::finalx = finalx;
}

void cylinder::setNumberOfBins(int numberOfBins) {
    number_of_bins = numberOfBins;
}

void cylinder::reset_bins() {
    bins.clear();
    for(int i = 0; i < number_of_bins; i++) bins.push_back(0);
}

void cylinder::add_to_bin(int bin_number) {
    if ((bin_number < bins.size())&&(bin_number >= 0)) {
        bins[bin_number] = bins[bin_number] + 1;
    }
}

void cylinder::add_to_bin(float x) {
    add_to_bin(convert_x_to_bin(x));
}

int cylinder::convert_x_to_bin(float x) {
    if ((x < startx)||(x > finalx)) {
        return -1;
    } else {
        return (int) floor((x - startx)*number_of_bins/(finalx - startx));
    }
}

void cylinder::print() {
    for (int i = 0; i < number_of_bins; i++) {
        cout << bins[i] << ", ";
    }
    cout << "\b\b\n";
}

float cylinder::convert_bin_to_x(int bin_number) {
    return ((float) bin_number + .5)*bin_size() + startx;
}

float cylinder::bin_size() {
    return (finalx - startx)/number_of_bins;
}

float cylinder::meniscus() {
    float numerator = meniscus_numerator();
    float denominator = meniscus_denominator();
    return numerator/denominator;
}

float cylinder::p_naught() {
    return *max_element(bins.begin(), bins.end());
}

float cylinder::meniscus_numerator() {
    float sum = 0;
    for (int i = 0; i < number_of_bins; i++){
        sum = sum + density_difference_squared(i)*convert_bin_to_x(i)*pow(bins[i], 2);
    }
    return sum;
}

float cylinder::meniscus_denominator() {
    float sum = 0;
    for (int i = 0; i < number_of_bins; i++){
        sum = sum + density_difference_squared(i)*pow(bins[i], 2);
    }
    return sum;
}

float cylinder::density_difference_squared(int bin) {
    return pow(bins[bin] - p_naught(), 2);
}


