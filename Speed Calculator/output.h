//
// Created by Robert Starr on 11/10/19.
//

#ifndef NEW_SPEED_CALCULATOR_OUTPUT_H
#define NEW_SPEED_CALCULATOR_OUTPUT_H
#include <string>

using namespace std;

void output_file(string filename, string text_to_write);

#endif //NEW_SPEED_CALCULATOR_OUTPUT_H
