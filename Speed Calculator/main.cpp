#include <iostream>
#include <vector>
#include "input.h"
#include "functions.h"
#include "parameters.h"
#include "cylinder.h"
#include "output.h"

using namespace std;

int main() {
    string containing_folder = "../..";
    string variable_file = containing_folder + "/variables.txt";
    string data_folder = containing_folder + "/Frames";
    string final_output = "";
    string output_filename = containing_folder + "/Speed Calculator/meniscii.txt";

    float wall_start = get_variable_from_file("WALLSTART", variable_file);
    float tube_length = get_variable_from_file("HEIGHT", variable_file) - wall_start;
    int number_of_bins = NUMBER_OF_BINS;

    cylinder base_cylinder;
    base_cylinder.setStartx(wall_start);
    base_cylinder.setFinalx(wall_start + tube_length);
    base_cylinder.setNumberOfBins(number_of_bins);

    vector<string> files = list_files(data_folder);
    int timesteps = files.size();
    for (int i = 0; i < timesteps; i++){
        cout << i << "/" << timesteps << ":\t" << files[i] << "\n";
        vector<float> atom_x_coordinates = get_x_coordinates(files[i]);
        base_cylinder.reset_bins();
        int count = 0;
        for (int j = 0; j < atom_x_coordinates.size(); j++){
            base_cylinder.add_to_bin(atom_x_coordinates[j]);
        }
        float meniscus = base_cylinder.meniscus();
        string new_line = to_string(get_file_number(files[i])) + "\t" + to_string(meniscus) + "\n";
        final_output = final_output + new_line;
        cout << new_line;
    }

    output_file(output_filename, final_output);
    return 0;
}