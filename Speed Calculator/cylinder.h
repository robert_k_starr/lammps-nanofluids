//
// Created by Robert Starr on 11/10/19.
//

#ifndef NEW_SPEED_CALCULATOR_CYLINDER_H
#define NEW_SPEED_CALCULATOR_CYLINDER_H

#include <vector>

using namespace std;

class cylinder {
    private:
        float startx, finalx;
        int number_of_bins;
        vector<int> bins;
        int convert_x_to_bin(float x);
        float convert_bin_to_x(int bin_number);
        float bin_size();
        float p_naught();
        float meniscus_numerator();
        float meniscus_denominator();
        float density_difference_squared(int);

    public:
        void setFinalx(float finalx);
        void setStartx(float startx);
        void setNumberOfBins(int numberOfBins);
        void reset_bins();
        void add_to_bin(int);
        void add_to_bin(float);
        void print();
        float meniscus();
};


#endif //NEW_SPEED_CALCULATOR_CYLINDER_H
