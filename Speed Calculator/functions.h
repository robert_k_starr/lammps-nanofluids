//
// Created by Robert Starr on 11/9/19.
//

#ifndef NEW_SPEED_CALCULATOR_FUNCTIONS_H
#define NEW_SPEED_CALCULATOR_FUNCTIONS_H

#include <vector>

using namespace std;

vector<float> get_x_coordinates(string);
float get_type(string line);
float get_x(string);
int get_file_number(string);

#endif //NEW_SPEED_CALCULATOR_FUNCTIONS_H
