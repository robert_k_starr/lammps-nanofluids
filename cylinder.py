import sys, os, random
from PIL import Image, ImageDraw
from math import *

HEIGHT = 50.0
THICK = 20
LENGTH = 20
DEPTH = 20

WALLSTART = LENGTH/4

SCREENWIDTH = 800
SCREENHEIGHT = 600

RATIO = min(float(SCREENWIDTH)/(LENGTH), float(SCREENHEIGHT)/(2*THICK + HEIGHT))

SCREENWIDTH = int(LENGTH*RATIO)
SCREENHEIGHT = int((2*THICK + HEIGHT)*RATIO)

SCREENSIZE = (SCREENWIDTH, SCREENHEIGHT)

ATOMRADIUS = 2

# Particle Masses
OMASS = 15.9994
HMASS = 1.008

# Particle Charges
OCHARGE = -.834
HCHARGE = .417
SICHARGE = .9

HBONDDISTANCE = .9572
HBONDANGLE = 104.52
HANGLETYPE = 1

WATERBOND = 1
WATERDIST = 3 ## 4

SILICABOND = 2
OXYGENBOND = 3

SIDIST = 3.1

SITYPE = 1
OTYPE = 2
WATERTYPE = 2
METHTYPE = 3
HTYPE = 4

#COLORS
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)
BROWN = (139, 69, 19)

PI = pi
TOTALHEIGHT = HEIGHT + 2*THICK

XMIN = 0.0
XMAX = LENGTH

YMIN = 0.0
YMAX = TOTALHEIGHT

ZMIN = 0.0
ZMAX = DEPTH

random.seed()

atoms = []
bonds = []
angles = []

def color(num):
	if num == 1:
		return RED
	if num == 2:
		return BLUE
	if num == 3:
		return YELLOW
	if num == 4:
		return GREEN
	return WHITE

def distance(coord1, coord2):
	if len(coord1) != len(coord2):
		return False
	vector = []
	for values in range(0, len(coord1)):
		vector.append(coord1[values] - coord2[values])
	sqsum = 0
	for values in vector:
		sqsum = sqsum + pow(values, 2)
	return sqrt(sqsum)

def midpoint(coord1, coord2):
	if len(coord1) != len(coord2):
		return False
	coords = []
	for value in range(0, len(coord1)):
		coords.append((coord1[value] + coord2[value])/2)
	return coords

def cart(r, angle, z = 0):
	x = r*cos(angle)
	y = r*sin(angle)
	return(x, y, z)

class particle:
	def __init__(self, pos, type = None, charge = 0):
		self.x = pos[0]
		self.y = pos[1]
		if len(pos) == 2:
			self.z = 0.0
		else:
			self.z = pos[2]
		self.dipole = [0, 0, 0]
		self.coords = (self.x, self.y, self. z)
		if type == None:
			self.type = 1
			if self.x > XMAX/2:
				self.type = 2
		else:
			self.type = type
		self.charge = charge
		self.molecule = 0

def addcords(coords1, coords2):
	if (len(coords1) != len(coords2)):
		print coords1
		print coords2
		return 0
	result = ()
	for value in range(len(coords1)):
		result = result + (coords1[value] + coords2[value],)
	return result
	
def water(atom):
	coords = atom.coords
	angle = random.randint(0, 20000)*pi/10000
	part1 = addcords(cart(HBONDDISTANCE, angle), coords)
	part2 = addcords(cart(HBONDDISTANCE, angle + HBONDANGLE*pi/180), coords)
	atoms.append(particle(part1, HTYPE))
	atoms.append(particle(part2, HTYPE))
	atom.charge = OCHARGE
	atoms[-2].charge = HCHARGE
	atoms[-1].charge = HCHARGE
	bonds.append(str(WATERBOND) + " " + str(atoms.index(atom) + 1) + " " 
		+ str(atoms.index(atoms[-2]) + 1))
	bonds.append(str(WATERBOND) + " " + str(atoms.index(atom) + 1) + " " 
		+ str(atoms.index(atoms[-1]) + 1))
	angles.append(str(HANGLETYPE) + " " + str(atoms.index(atoms[-2]) + 1) + " " 
		+ str(atoms.index(atom) + 1) + " " + str(atoms.index(atoms[-1]) + 1))

def printatom(number, atom):
	text = str(number) + " " + str(atom.type) + " "
	for value in atom.coords:
		text = text + str(value) + " "
	text = text + str(atom.molecule) + " "
	text = text + str(atom.charge) + " "
	text = text + str(atom.molecule) + " "
	return text
	
def recvariable(file, name, value):
	file.write("variable " + name + " equal " + str(value) + "\n")

def hexfill(start, end, spacing, type, charge = 0, z = 0):
	count = 0
	tempbox = []
	
	xmin = min(start[0], end[0])
	ymin = min(start[1], end[1])
	
	xmax = max(start[0], end[0])
	ymax = max(start[1], end[1])
	
	x = xmin
	y = ymin
	
	truexmax = xmin
	trueymax = ymin
	
	shift = False
	
	while (y < ymax):
		while (x < xmax):
			if type == SITYPE:
				if not shift and (round(((x - xmin)/spacing)%3) == 1):
					x = x + spacing
				if shift and (round(((x - spacing/2 - xmin)/spacing)%3) == 2):
					x = x + spacing
				if x > xmax:
					break
				for atom in atoms:
					if (atom.type == SITYPE) and (distance(atom.coords, (x, y, z)) < 1.01*spacing): # Frank says this is not good
						midpt = midpoint(atom.coords, (x, y, z))
						atoms.append(particle(midpt, OTYPE, -.45)) ## -.45 = OCharge in SiO2
						tempbox.append(atoms[-1])
			atoms.append(particle((x, y, z), type, charge))
			tempbox.append(atoms[-1])

			if x > truexmax:
				truexmax = x
			if y > trueymax:
				trueymax = y
				
			if type == WATERTYPE:
				water(atoms[-1])
			x = x + spacing		
			count = count + 1


		x = xmin
		
		if shift == False:
			shift = True
			x = x + spacing/2
		else:
			shift = False
			
		y = y + spacing*sqrt(3)/2

	if type == SITYPE:
		for atom in tempbox:
			if atom.type == SITYPE:
				if atom.x + .1 > truexmax or atom.y + .1 > trueymax or atom.x - .1 < xmin or atom.y - .1 < ymin:
					atoms.remove(atom)
					count = count - 1
		for oxygen in tempbox:
			partner = False
			if oxygen.type == OTYPE:
				for silicon in atoms:
					if silicon.type == SITYPE:
						if distance(oxygen.coords, silicon.coords) < spacing:
							partner = True
							continue
				if partner == False:
					atoms.remove(oxygen)
					count = count - 1
				
	print "Filled in " + str(count) + " atoms!"

def checkbox(atom):
	if atom.x < XMIN:
		return False
	if atom.x > XMAX:
		return False
	if atom.y < YMIN:
		return False
	if atom.y > YMAX:
		return False
	return True

def plot(atom, radius, drawing, ratio = RATIO, offset = (0, 0)):
	xstart = (1 + atom.x - offset[0])*ratio - radius
	ystart = (1 + atom.y - offset[1])*ratio - radius
	xend = (1 + atom.x - offset[0])*ratio + radius
	yend = (1 + atom.y - offset[1])*ratio + radius
	coords = (xstart, ystart, xend, yend)
	draw = ImageDraw.Draw(drawing)
	draw.ellipse([(xstart, ystart), (xend, yend)], fill=color(atom.type))

def screenshot(frame):
	screen = Image.new('RGB', SCREENSIZE, BLACK)
	for atom in atoms:
			plot(atom, ATOMRADIUS, screen)
	screen.save("./test" + str(frame).zfill(5) + ".jpg", "JPEG")
	del screen

layer = 0

## BOUNDARY SANDWICH BOTTOM SLICE
while (layer < THICK):
	hexfill((WALLSTART, 0), (LENGTH, HEIGHT + 2*THICK), SIDIST, SITYPE, SICHARGE, 0)

## BOUNDARY TOP + BOTTOM
while (layer < DEPTH + THICK):
	hexfill((WALLSTART, 0), (LENGTH, THICK), SIDIST, SITYPE, SICHARGE, layer)
	hexfill((WALLSTART, HEIGHT + THICK), (LENGTH, HEIGHT + 2*THICK), SIDIST, SITYPE, SICHARGE, layer)
	layer = layer + SIDIST

## BOUNDARY SANDWICH TOP SLICE
while (layer < DEPTH + 2*THICK):
	hexfill((WALLSTART, 0), (LENGTH, HEIGHT + 2*THICK), SIDIST, SITYPE, SICHARGE, layer)

TOTALCHARGE = 0
OCOUNT = 0 

for atom in atoms:
	TOTALCHARGE = TOTALCHARGE + atom.charge

EXCHARGE = TOTALCHARGE/len(atoms)

for atom in atoms:
	atom.charge = atom.charge - EXCHARGE

print "Bonding..."

for oxygen in atoms:
	if (oxygen.type == OTYPE):
		for silicon in atoms:
			if (silicon.type == SITYPE):
				if distance(oxygen.coords, silicon.coords) < 1.01*SIDIST/2:
					bonds.append(str(SILICABOND) + " " + str(atoms.index(oxygen) + 1) + " " 
						+ str(atoms.index(silicon) + 1))

print "Done bonding (finally)"

## INSIDES
layer = THICK
while( layer < DEPTH + THICK - WATERDIST/2 ):
	hexfill((WALLSTART/5, 1), (WALLSTART - WATERDIST, HEIGHT + 2*THICK - 2), WATERDIST, WATERTYPE, z = layer)
	hexfill((WALLSTART, THICK + 2), (WALLSTART + XMAX/5, HEIGHT + THICK - 2), WATERDIST, WATERTYPE, z = layer)
	layer = layer + WATERDIST
#hexfill((XMAX/2 + 4, THICK), (XMAX, HEIGHT + THICK), 500, METHTYPE)

TOTALCHARGE = 0

OCOUNT = 0
HCOUNT = 0
SICOUNT = 0
UNKNOWN = 0

for atom in atoms:
	if not checkbox(atom):
		print "Removed atom type " + str(atom.type) + " Coords: " + str(atom.coords) + "Charge: " + str(atom.charge)
		atoms.remove(atom)

# 	else:
# 		TOTALCHARGE = TOTALCHARGE + atom.charge
# 		if atom.type == OTYPE:
# 			OCOUNT = OCOUNT + 1
# 		elif atom.type == HTYPE:
# 			HCOUNT = HCOUNT + 1
# 		elif atom.type == SITYPE:
# 			SICOUNT = SICOUNT + 1
# 		else:
# 			UNKNOWN = UNKNOWN + 1	
# 			print atom.type + "\n"

# print "TOTAL CHARGE IS: " + str(TOTALCHARGE) + "\n\n"
# print "OXYGENS:\t" + str(OCOUNT)
# print "HYDROGENS:\t" + str(HCOUNT)
# print "SILICONS:\t" + str(SICOUNT)
# 
# EXCESS = OCOUNT - HCOUNT/2 - 2*SICOUNT
# 
# print EXCESS
# print TOTALCHARGE/.45

variablefile = open('variables.txt', 'w')
variablefile.truncate()

recvariable(variablefile, 'NEGRADIUS', 0.0)
recvariable(variablefile, 'POSRADIUS', float(LENGTH))
recvariable(variablefile, 'HEIGHT', float(TOTALHEIGHT))
region = str(0) + " " + str(XMAX) + " " 
region = region + str(THICK - 1) + " " + str(HEIGHT + THICK + 1) + " "
region = region + str(SIDIST - 1) + " " + str(layer - WATERDIST)
variablefile.write("region temp1 block " + region + "\n")

reservoir = str(0) + " " + str(WALLSTART - 1) + " "
reservoir = reservoir + str(0) + " " + str(YMAX) + " "
reservoir = reservoir + str(ZMIN) + " " + str(ZMAX)
variablefile.write("region temp2 block " + reservoir + "\n")

variablefile.write("region movers union 2 temp1 temp2")

zaxis = str(0) + " " + str(XMAX) + " "
zaxis = zaxis + str(0) + " " + str(YMAX) + " "
zaxis = zaxis + str((ZMAX - WATERDIST)/2) + " " + str((ZMAX + WATERDIST/2))

variablefile.write("region zaxis block " + zaxis + "\n")

variablefile.write("region display intersect 2 movers zaxis\n")

variablefile.close()
	
editfile = open("nanofile.txt", "w")

editfile.truncate()

editfile.write("LAMMPS Data File \n\n")
editfile.write(str(len(atoms)) + " atoms\n")
editfile.write(str(len(bonds)) + " bonds\n")
editfile.write(str(len(angles)) + " angles\n")
#editfile.write("1000 extra bond per atom\n") ## THESE NEED MODIFICATION
editfile.write("4 atom types\n") ## THESE NEED MODIFICATION
editfile.write("3.0 bond types\n\n") ## THESE NEED MODIFICATION
editfile.write("1.0 angle types\n\n") ## THESE NEED MODIFICATION

editfile.write(str(XMIN - 2) + " " + str(XMAX + 2) + " xlo xhi\n")
editfile.write(str(YMIN - 2) + " " + str(YMAX + 2) + " ylo yhi\n")
editfile.write(str(ZMIN - 2) + " " + str(ZMAX + 2) + " zlo zhi\n\n")

editfile.write("Atoms\n\n")

atomnumber = 0

for atom in atoms:
	atomnumber = atomnumber + 1
	editfile.write(printatom(atomnumber, atom) + "\n")

editfile.write("\nBonds\n\n")
	
for lines in bonds:
	editfile.write(str(bonds.index(lines)) + " " + lines + "\n")
	
editfile.write("\nAngles\n\n")

for lines in angles:
	editfile.write(str(angles.index(lines)) + " " + lines + "\n")

editfile.close()

#screenshot(1)