import os
import datetime
import sys
from shutil import *

tacc = False
filename = "lmp_mpi"
folder = "Nanofluids"

if "tacc" in sys.argv:
	tacc = True
	filename = "lmp_knl"
	folder = "lammps-nanofluids"

for files in os.listdir("./LAMMPS Modifiers/"):
	if files[0] != '.':
		copy('LAMMPS Modifiers/' + files, '../lammps/src/')

os.chdir("../lammps/src")

if "make" in sys.argv:
 os.system("make yes-mc")
 os.system("make yes-user-misc")
 os.system("make yes-molecule")
 os.system("make yes-kspace")
 os.system("make yes-rigid")
 os.system("make yes-manybody")
 os.system("make yes-user-meamc")

if "build" in sys.argv:
	if tacc:
		os.system("make knl")
	else:
		os.system("make -j 2 mpi")

if "clean" in sys.argv:
 os.system("make clean-all")
 os.system("make no-mc")
 os.system("make no-user-misc")
 os.system("make no-molecule")
 os.system("make no-kspace")
 os.system("make no-rigid")
 os.system("make no-manybody")
 os.system("make no-user-meamc")

os.chdir("../..")

copy('lammps/src/' + filename, folder + '/')
