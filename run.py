# Process is:
# 1) Use Cylinder C++ Program to make cylinder
# 2) python run.py water silica
# 3) Use Roughness C++ Program
# 4) python run.py prep
# 5) python run.py run
# 6) (Optional) python run.py repeat

import os
import sys
import datetime
import math
import time
from shutil import *

deletebox = []

filename = 'in.nanoflow'

run = False
frames = False

# for files in os.listdir('./Frames'):
# 	if ('.') not in files:
# 		rmtree('./Frames/' + files)
# 	elif files[0] != ('.'):
# 		deletebox.append(files)
# 
# for files in deletebox:
# 	os.remove('./Frames/' + files)

os.system("rm -rf ./Frames/*")
os.system("rm -rf ./MiddleFrames/*")
	
copy('./C++ Cylinder/nanofile.txt',
	'./nanofile.txt')
copy('./C++ Cylinder/variables.txt',
	'./variables.txt')
 	
# for file in os.listdir('./'):
# 	if ((file[-4:-1] + file[-1])=='.txt') or (file == 'in.nanoflow') or (file == 'lmp_mpi'):
# 		copy('./' + file, 
# 			'/Users/Robert/Google Drive/Marder/Nanoflow/LAMMPS-Nanofluidics Collaboration/LAMMPS Files/' + file)

for input in sys.argv:
	if (input == "run.py"):
		continue
	elif (input == "build"):
		os.system("cd C++\ Cylinder/\nmake clobber\nmake clean run")
		copy('./C++ Cylinder/nanofile.txt',
			'./nanofile.txt')
		copy('./C++ Cylinder/variables.txt',
			'./variables.txt')

	# elif (input == "clean"):
	# 	dir = '/Users/Robert/Google Drive/Marder/Nanoflow/LAMMPS-Nanofluidics Collaboration/Scratch/'

	# 	for folder in os.listdir(dir):
	# 		if folder[0] == "2":
	# 			rmtree(dir + folder)
	
	elif (input == "run"):
		run = True
		
	elif (input == "water"):
		os.system("mpirun -np 2 lmp_mpi < in.water")
	
	elif (input == "silica"):
		os.system("python silicapotentials.py")
		os.system("mpirun -np 2 lmp_mpi < in.silica")
	
	elif (input == "hydroxylate"):
		os.system("mpirun -np 1 lmp_mpi < in.hydroxylate")
		
	elif (input == "prep"):
		os.system("mpirun -np 2 lmp_mpi < in.nanoflow")
	
	elif (input == "frames"):
		frames = True
	
	elif (input == "repeat"):
		os.system("mpirun -np 2 lmp_mpi < in.repeat")
		
	else:
		os.system("mpirun -np 2 lmp_mpi < " + input )
		


if run == False and frames == False:
	sys.exit()

atomtypes = 0
bondtypes = 0

text = open(filename, "r")
width = 0
height = 0
depth = 0
scale = 0

if run and frames:
	os.system("mpirun -np 2 lmp_mpi < in.run &")

elif run:
	os.system("mpirun -np 2 lmp_mpi < in.run")

dir = '/Users/Robert/Google Drive/Marder/Nanoflow/LAMMPS-Nanofluidics Collaboration/Scratch/'
currenttime = datetime.datetime.now()

folder = currenttime.isoformat()

dir = dir + folder + '/'

# os.mkdir(dir)

while True and frames:
	time.sleep(1)
	if os.listdir('./Frames'):
		for files in os.listdir('./Frames'):
			if files[0] != ('.'):
				os.rename('./Frames/' + files, dir + files)