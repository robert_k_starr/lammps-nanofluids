#include <iostream>
#include <stdio.h>
#include <cstring>
#include <stdlib.h>
#include <math.h>

#include "functions.hpp"

using namespace std;

void cleanfolder(string folder){
	string temp = "exec rm -r " + folder + "*";
	system(temp.c_str());
}

string convertline(string line){
	string temp[5];
	string converted;
	int place = 0;
	
	for (int i = 0; i < line.length(); i++) {
		if (isspace(line[i])){
			place++;
			continue;
		}
		temp[place] = temp[place] + line[i];
	}
	int sign = 1;
	if (stof(temp[3]) < 0) sign = -1;
	temp[3] = to_string(sign*sqrt(pow( stof(temp[3]), 2) + pow( stof(temp[4]),2)));
	temp[4] = "0";
	
	for (int i = 0; i < 5; i++) converted = converted + temp[i] + " ";
	converted = converted + "\n";
	return converted;	
}