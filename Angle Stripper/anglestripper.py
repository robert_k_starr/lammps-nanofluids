import os, math

if not os.path.exists('Frames/NoAngle'):
	os.makedirs('Frames/NoAngle')

for files in os.listdir('Frames/NoAngle'):
	os.remove("Frames/NoAngle/" + files)

totalfiles = 0
currentfile = 0

for files in os.listdir('Frames/'):
	if(files[-4:-1] + files[-1] == ".txt"):
		totalfiles = totalfiles + 1

for files in os.listdir('Frames/'):
	changefiles = False
	if(files[-4:-1] + files[-1] == ".txt"):
		currentfile = currentfile + 1
		print "Currently on file " + str(currentfile) + "/" +str(totalfiles)
		readfile = open("Frames/" + files, 'r')
		writefile = open("Frames/NoAngle/" + files, 'w')
		for line in readfile:
			if changefiles:
				spaces = [line.find(" ") + 1, 0, 0, 0]
				for value in range(1,4):
					spaces[value] = line.find(" ", spaces[value - 1], len(line)) + 1
				x = float(line[spaces[2]:spaces[3]])
				y = float(line[spaces[3]:])
				r = math.sqrt(pow(x,2) + pow(y,2))
				if (y < 0):
					r = -r
				newline = line[:spaces[2]] + str(r) + " 0 \n"
				writefile.write(newline)
			else:
				writefile.write(line)
			if (line[0:25] == "ITEM: ATOMS id type x y z"):
				changefiles = True
